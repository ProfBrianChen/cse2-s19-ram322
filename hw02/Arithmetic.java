//////////////
//// CSE 02 Arithmetic 
//// 05.02.19 Rafaela Mantoan Borges
//// Given the quantity of each item bought, the cost per item and the PA sales tax percentage, this program should: 
// Calculate and print out the total cost of each kind of item (i.e. total cost of pants, etc)
// Calculate and print out the sales tax charged on each kind of item (i.e. sales tax charged on belts)
// Calculate and print out the total cost of purchases (before tax)
// Calculate and print out the total sales tax 
// Calculate and print out the total paid for this transaction, including sales tax

public class Arithmetic {
  //Main Method 
  public static void main(String[] args) {
    
    //Input data
    
    //Pants
    int NumberPants = 3; //Declares and assigns the number of pants bought to an integer variable
    double CostPerPants = 34.98; //Declares and assigns the price of one pair of pants to a double variable
    
    //Shirts
    int NumberShirts = 2; //Declares and assigns the number of shirts bought to an integer variable
    double CostPerShirt = 24.99; //Declares and assigns the price of one shirt to a double variable
    
    //Belts
    int NumberBelts = 1; //Declares and assigns the number of belts bought to an integer variable
    double CostPerBelt = 33.99; //Declares and assigns the price of one belt to a double variable
    
    //Tax rate 
    double PaSalesTax = 0.06; //Declares and assigns the PA sales tax percentage to a double variable
    
    //Output data
    double TotalCostPants, 
    TotalCostShirts, TotalCostBelts; //Declares the total cost of pants, shirts and belts as double variables 
    double SalesTaxPants, 
    SalesTaxShits, SalesTaxBelts; //Declares the sales tax charged on pants, shirts and belts as double variables
    double TotalCostPurchases, 
    TotalSalesTax, TotalCostPaid; //Declares the total cost of purchases (without tax), the total sales tax and the total paid for the transaction (including sales tax) as double variables
    
    //Runs calculations and stores values
    TotalCostPants = NumberPants*CostPerPants; //Multiplies accessed variables and stores the result in TotalCostPants (the total cost of pants)
    TotalCostShirts = NumberShirts*CostPerShirt; //Multiplies accessed variables and stores the result in TotalCostShirts (the total cost of shirts)
    TotalCostBelts = NumberBelts*CostPerBelt; //Multiplies accessed variables and stores the result in TotalCostBelts (the total cost of belts) 
    SalesTaxPants = TotalCostPants*PaSalesTax; //Multiplies the total cost of pants by the PA sales tax percentage and stores the result in SalesTaxPants (sales tax charged on pants)
    SalesTaxShits = TotalCostShirts*PaSalesTax; //Multiplies the total cost of shirts by the PA sales tax percentage and stores the result in SalesTaxShits (sales tax charged on shirts)
    SalesTaxBelts = TotalCostBelts*PaSalesTax; //Multiplies the total cost of belts by the PA sales tax percentage and stores the result in SalesTaxBelts (sales tax charged on belts)
    
    //Converts the results to two decimal places
    SalesTaxPants = (int) (SalesTaxPants*100); //Multiplies the double variable, SalesTaxPants, by 100 and casts it to an integer variable using explict casting 
    SalesTaxPants /= 100; //Divides SalesTaxPants by 100 to convert the value to two decimal places
    SalesTaxShits = (int) (SalesTaxShits*100); //Multiplies the double variable, SalesTaxShits, by 100 and casts it to an integer variable using explicit casting
    SalesTaxShits /= 100; //Divides SalesTaxShits by 100 to convert the value to two decimal places
    SalesTaxBelts = (int) (SalesTaxBelts*100); //Multiplies the double variable, SalesTaxBelts, by 100 and casts it to an integer variable using explicit casting
    SalesTaxBelts /= 100; //Divides SalesTaxBelts by 100 to convert the value to two decimal places
    
    //Prints out output data
    System.out.println("The total cost of pants was $" + TotalCostPants + 
                       " and the sales tax charged on them was $" + SalesTaxPants + "."); //Prints out the total cost of pants and the sales tax charged on pants
    System.out.println("The total cost of shirts was $" + TotalCostShirts + 
                       " and the sales tax charged on them was $" + SalesTaxShits + "."); //Prints out the total cost of shirts and the sales tax charged on shirts
    System.out.println("The total cost of belts was $" + TotalCostBelts + 
                       " and the sales tax charged on them was $" + SalesTaxBelts + "."); //Prints out the total cost of belts and the sales tax charged on belts
    
    //Runs calculations and stores values
    TotalCostPurchases = TotalCostPants+TotalCostShirts+TotalCostBelts; //Adds accessed variables and stores the result of the operation in TotalCostPurchases (total cost of the purchases before sales tax)
    TotalSalesTax = SalesTaxPants+SalesTaxShits+SalesTaxBelts; //Adds accessed variables and stores the result of the operation in TotalSalesTax (total sales tax charged on the purchases)
    TotalCostPaid = TotalCostPurchases+TotalSalesTax; //Adds accessed variables and stores the result of the operation in TotalCostPaid (total paid for the transaction including sales tax)
    
    //Prints out output data
    System.out.println("The total cost of the purchases (before sales tax) was $" + TotalCostPurchases + "."); //Prints out the total cost of purchases (before sales tax) 
    System.out.println("The total sales tax charged was $" + TotalSalesTax + "."); //Prints out the total sales tax charged on the purchases 
    System.out.println("The total cost paid for this transaction (including sales tax) was $" + TotalCostPaid + "."); //Prints out the total paid for the trasaction (including sales tax)
    
  } //End of Main Method
  
} //End of Class