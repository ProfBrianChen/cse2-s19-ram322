//////////////
//// CSE 02 Box Volume
//// 02.12.19 Rafaela Mantoan Borges
//// Given a box with unknown dimensions, which should be stored in doubles, this program should:
// Prompt the user for the dimensions of a box: length, width and height
// Calculate and print out the volume inside the box

import java.util.Scanner; //Imports Scanner class (import statement)

public class BoxVolume {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner( System.in ); //Declares and constructs an instance
    
    //Input data 
    System.out.print("The width side of the box is: "); //Prompts the user for the width of the box
    double BoxWidth = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called BoxWidth
    System.out.print("The length of the box is: "); //Prompts the user for the length of the box
    double BoxLength = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called BoxLength
    System.out.print("The height of the box is: "); //Prompts the user for the height of the box
    double BoxHeight = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called BoxHeight
    
    //Output data 
    double BoxVolume = BoxWidth*BoxLength*BoxHeight; //Calculates the volume of the box by multiplying the accessed variables and stores it in a double variable called BoxVolume
    
    //Prints out output data
    System.out.println("The volume inside the box is: " + BoxVolume); //Prints out the volume inside the box 
    
  } //End of main method
  
} //End of class