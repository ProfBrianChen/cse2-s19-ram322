//////////////
//// CSE2 Straight
//// 05/02/19 Rafaela Mantoan Borges

import java.util.Arrays; //import Arrays class
import java.lang.Math; //import Math class

public class Straight {
  
  //Method for generating and shuffling a deck of cards:
  public static int[] generateSuffledDeck () { //This method returns an integer array 
    int[] deckOfCards = new int[52]; //Declares and allocates space for an array of length 52
    for (int i = 0; i < deckOfCards.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the array 
      deckOfCards[i] = i; //Initializes each member
    } //End of for loop 
    //Shuffles the deck of cards:
    for (int j = 0; j < deckOfCards.length; j++) { //Initializes a counter incrementing it by one until it is equal to the length of the array
      int target = (int) (Math.random()*deckOfCards.length); //Generates a random integer 
      int temp = deckOfCards[target]; //Declares and initializes an integer 
      deckOfCards[target] = deckOfCards[j]; //Swaps the values of the members 
      deckOfCards[j] = temp; //Swaps the values of the members 
    } //End of if statement 
    return deckOfCards; //Returns array
  } //End of generateSuffledDeck method
  
  //Method for drawing the first five cards of the deck of cards:
  public static int[] drawFiveCards (int[] deckOfCards) { //This method accepts an integer array and returns another one
    int[] myHand = new int[5]; //Declares and allocates space for an array of length 5
    for (int i = 0; i < myHand.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the new array 
      myHand[i] = deckOfCards[i]; //Initializes each member 
    } //End of for loop
    //Coverts all members of the array to numbers within the range 0(Ace)-12(King):
    for (int i = 0; i < myHand.length; i++) { //Initializes and declares a counter incrementing it by one until it is equal to the length of the new array 
      int remainder = myHand[i]%13; //Declares an integer and stores the value of the remainder in it 
      switch (remainder) { //Evaluates the remainder
        case 0: //If remainder equals 0:
          myHand[i] = 0; //Sets the value of the member of the array to 0 (ace)
          break; //Breaks out of the condition
        case 1: //If remainder equals 1:
          myHand[i] = 1; //Sets the value of the member of the array to 1
          break; //Breaks out of the condition
        case 2: //If remainder equals 2:
          myHand[i] = 2; //Sets the value of the member of the array to 2
          break; //Breaks out of the condition
        case 3: //If remainder equals 3:
          myHand[i] = 3; //Sets the value of the member of the array to 3
          break; //Breaks out of the condition
        case 4: //If remainder equals 4:
          myHand[i] = 4; //Sets the value of the member of the array to 4
          break; //Breaks out of the condition
        case 5: //If remainder equals 5:
          myHand[i] = 5; //Sets the value of the member of the array to 5
          break; //Breaks out of the condition
        case 6: //If remainder equals 6:
          myHand[i] = 6; //Sets the value of the member of the array to 6
          break; //Breaks out of the condition
        case 7: //If remainder equals 7:
          myHand[i] = 7; //Sets the value of the member of the array to 7
          break; //Breaks out of the condition
        case 8: //If remainder equals 8:
          myHand[i] = 8; //Sets the value of the member of the array to 8
          break; //Breaks out of the condition
        case 9: //If remainder equals 9:
          myHand[i] = 9; //Sets the value of the member of the array to 9
          break; //Breaks out of the condition
        case 10: //If remainder equals 10:
          myHand[i] = 10; //Sets the value of the member of the array to 10 (jack)
          break; //Breaks out of the condition
        case 11: //If remainder equals 11:
          myHand[i] = 11; //Sets the value of the member of the array to 11 (queen)
          break; //Breaks out of the condition
        case 12: //If remainder equals 12:
          myHand[i] = 12; //Sets the value of the member of the array to 12 (king)
          break; //Breaks out of the condition
      } //End of switch statement
    } //End of for loop 
    return myHand; //Returns new array 
  } //End of drawFiveCards method 
  
  //Method to find kth lowest number:
  public static int linearSearch (int[] myHand, int k) { //This method accepts an integer array and and integer and returns an integer 
    int index = 0; //Declares and initializes an integer to represent the index 
    if (k > 5 || k <= 0) { //If k is greater than 5 or less than or equal to 0
      return -1; //Returns error
    } else { //If not:
      //Sorts the array in ascending order by using insertion sort:
      for (int i = 1; i < myHand.length; i++) { 
        for (int j = i ; j > 0 ; j--) {
          if (myHand[j] < myHand[j-1]) {
            int temp = myHand[j];
            myHand[j] = myHand[j-1];
            myHand[j-1] = temp;
          }
        }
      }
      //Finds the kth lowest number by using linear search:
      for (int l = 0; l < myHand.length; l++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the array
        if (l == (k-1)) { //If l equals k-1:
          index = l; //Sets index to l
          break; //Breaks out of the loop
        } //End of if statement 
      } //End of for loop 
    } //End of if statement 
    return myHand[index]; //Returns myHand[index]
  } //End of linearSearch method 
  
  //Method for checking if you have a straight: 
  public static boolean straightSearch (int[] myHand) {
    int card = linearSearch(myHand, 1); //Declares an integer and stores the value of the lowest card in it 
    boolean straight = false; //Declares a boolean 
    for (int i = 2; i <= 5; i++) { //Declares and initializes a counter incrementing it by 1 until it is greater than 5  
      if (linearSearch(myHand, 1) == 12) { //If the lowest card equals 12 (king):
        break; //Breaks out of the loop 
      } else { //If not:
        if (i != 5) { //If i is different than 5:
          if ((linearSearch(myHand, i) == card+1) && (linearSearch(myHand, i) != 12)) { //If the ith lowest numer equals the i-1th lowest number + 1 and is different than 12 (king)
            card = linearSearch(myHand, i); //Sets card to the ith lowest number 
          } else { //If not:
            break; //Breaks out of the loop 
          } //End of if statement  
        } else if (i == 5) { //If i equals 5
          if ((linearSearch(myHand, i) == card+1)) { //If the fifth lowest number equals the fourth lowest:
            straight = true; //Sets straight to true 
          } else { //If not:
            break; //Breaks out of the loop 
          } //End of if statement 
        } //End of if statement  
      } //End of if statement 
    } //End of for loop 
    return straight; //Returns straight 
  } //End of straightSearch method
  
  public static void main (String[] args) {
    //Finds the probability of drawing a straight:
    int count = 0; //Declares and initializes a counter 
    for (int i = 1; i <= 1000000; i++) { //Declares and initializes a counter incrementing it by 1 until it is greater tham one million
      int[] suffledDeck = generateSuffledDeck(); //Calls generateSuffledDeck method 
      int[] myHand = drawFiveCards(suffledDeck); //Calls drawFiveCards method 
      if (straightSearch(myHand)) { //Calls straightSearch method and if it is true:
        count++; //Increments counter by one
      } //End if statement 
    } //End of for loop 
    System.out.println("The number of straights in one million tries is " + count); //Prints out the statement to terminal window
    double percentage = (double)(count*100)/1000000; //Converts the number to a percentage 
    System.out.println("The chance of drawing a straight is " + percentage + "%"); //Prints out the statement to terminal window
  } //End of main method 
  
} //End of class 