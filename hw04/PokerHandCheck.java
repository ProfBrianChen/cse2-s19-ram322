//////////////
//// CSE2 Poker Hand Check 
//// 02.19.19 Rafaela Mantoan Borges
//// Given that you randomly drew five cards, each from a different shuffled deck, this program should:
// Use the Math class to genarate a random number
// Detect whether the five cards contain a pair, two pairs, or three of a kind
// Report that you have a “high card hand” if none of these special hands exist 

//Import statement 
import java.lang.Math; //Imports Math class

public class PokerHandCheck {
  //Main method for every Java program 
  public static void main(String[] args) {
    
    //Generates five random number from 1 to 52 (inclusive) (input data)
    int RandomNumber1 = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for card 1 from 1 to 52 and declares it as integer variable 
    int RandomNumber2 = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for card 2 from 1 to 52 and declares it as integer variable
    int RandomNumber3 = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for card 3 from 1 to 52 and declares it as integer variable
    int RandomNumber4 = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for card 4 from 1 to 52 and declares it as integer variable
    int RandomNumber5 = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for card 5 from 1 to 52 and declares it as integer variable
    
    //Creates string variables 
    String Suit1 = ""; //Declares the suit of the first card as a string variable 
    String Identity1 = ""; //Declares the identity of the first card as a string variable 
    String Suit2 = ""; //Declares the suit of the second card as a string variable
    String Identity2 = ""; //Declares the identity of the second card as a string variable
    String Suit3 = ""; //Declares the suit of the third card as a string variable
    String Identity3 = ""; //Declares the identity of the third card as a string variable
    String Suit4 = ""; //Declares the suit of the fourth card as a string variable
    String Identity4 = ""; //Declares the identity of the fourth card as a string variable
    String Suit5 = ""; //Declares the suit of the fifth card as a string variable
    String Identity5 = ""; //Declares the identity of the fifth card as a string variable
    
    //Creates a counter for each variable 
    int Ace = 0; //Declares the counter for cards that have ace as their identity as an integer variable 
    int King = 0; //Declares the counter for cards that have king as their identity as an integer variable 
    int Queen = 0; //Declares the counter for cards that have queen as their identity as an integer variable 
    int Jack = 0; //Declares the counter for cards that have jack as their identity as an integer variable 
    int Two = 0; //Declares the counter for cards that have two as their identity as an integer variable 
    int Three = 0; //Declares the counter for cards that have three as their identity as an integer variable 
    int Four = 0; //Declares the counter for cards that have four as their identity as an integer variable 
    int Five = 0; //Declares the counter for cards that have five as their identity as an integer variable 
    int Six = 0; //Declares the counter for cards that have six as their identity as an integer variable 
    int Seven = 0; //Declares the counter for cards that have seven as their identity as an integer variable 
    int Eight = 0; //Declares the counter for cards that have eight as their identity as an integer variable 
    int Nine = 0; //Declares the counter for cards that have nine as their identity as an integer variable 
    int Ten = 0; //Declares the counter for cards that have ten as their identity as an integer variable 
     
    //First card:
    //Assigns the suit name 
    if (RandomNumber1 <= 13) { //If the boolean expression that says that the random number for card 1 is less than or equal to 13 is true:
      Suit1 = "Diamonds"; //Assigns the suit of card 1 to diamonds 
    } else if (14 <= RandomNumber1 && RandomNumber1 <= 26) { //If the boolean expression that says that the random number for card 1 is between 14 and 26 is true:
      Suit1 = "Clubs"; //Assigns the suit of card 1 to clubs
    } else if (27 <= RandomNumber1 && RandomNumber1 <= 39) { //If the boolean expression that says that the random number for card 1 is between 27 and 39 is true:
      Suit1 = "Hearts"; //Assigns the suit of card 1 to hearts 
    } else if (40 <= RandomNumber1 && RandomNumber1 <= 52) { //If the boolean expression that says that the random number for card 1 is between 40 and 52 is true:
      Suit1 = "Spades"; //Assigns the suit of card 1 to spades 
    } //End of if statement
    
    //Assigns the card identity 
    int Remainder1 = RandomNumber1%13; //Calculates the remainder of the division between the random number for card 1 and 13 and declares it as an integer variable 
    switch (Remainder1) { //Evaluates the value of the remainder for card 1 
      case 0: //If the remainder for card 1 is 0:
        Identity1 = "King"; //Assigns the identity of card 1 to king 
        King++; //Increments the counter for cards that have king as their identity by one 
        break; //Breaks out of the condition 
      case 1: //If the remainder for card 1 is 1:
        Identity1 = "Ace"; //Assigns the identity of card 1 to ace 
        Ace++; //Increments the counter for cards that have ace as their identity by one
        break; //Breaks out of the condition
      case 2: //If the remainder for card 1 is 2:
        Identity1 = "2"; //Assigns the identity of card 1 to 2 
        Two++; //Increments the counter for cards that have two as their identity by one
        break; //Breaks out of the condition
      case 3: //If the remainder for card 1 is 3:
        Identity1 = "3"; //Assigns the identity of card 1 to 3 
        Three++; //Increments the counter for cards that have three as their identity by one
        break; //Breaks out of the condition
      case 4: //If the remainder for card 1 is 4:
        Identity1 = "4"; //Assigns the identity of card 1 to 4 
        Four++; //Increments the counter for cards that have four as their identity by one
        break; //Breaks out of the condition
      case 5: //If the remainder for card 1 is 5:
        Identity1 = "5"; //Assigns the identity of card 1 to 5 
        Five++; //Increments the counter for cards that have five as their identity by one
        break; //Breaks out of the condition
      case 6: //If the remainder for card 1 is 6:
        Identity1 = "6"; //Assigns the identity of card 1 to 6 
        Six++; //Increments the counter for cards that have six as their identity by one
        break; //Breaks out of the condition
      case 7: //If the remainder for card 1 is 7:
        Identity1 = "7"; //Assigns the identity of card 1 to 7 
        Seven++; //Increments the counter for cards that have seven as their identity by one
        break; //Breaks out of the condition
      case 8: //If the remainder for card 1 is 8:
        Identity1 = "8"; //Assigns the identity of card 1 to 8 
        Eight++; //Increments the counter for cards that have eight as their identity by one
        break; //Breaks out of the condition
      case 9: //If the remainder for card 1 is 9:
        Identity1 = "9"; //Assigns the identity of card 1 to 9 
        Nine++; //Increments the counter for cards that have nine as their identity by one
        break; //Breaks out of the condition
      case 10: //If the remainder for card 1 is 10:
        Identity1 = "10"; //Assigns the identity of card 1 to 10 
        Ten++; //Increments the counter for cards that have ten as their identity by one
        break; //Breaks out of the condition
      case 11: //If the remainder for card 1 is 11:
        Identity1 = "Jack"; //Assigns the identity of card 1 to jack 
        Jack++; //Increments the counter for cards that have jack as their identity by one
        break; //Breaks reak out of the condition
      case 12: //If the remainder for card 1 is 12:
        Identity1 = "Queen"; //Assigns the identity of card 1 to queen
        Queen++; //Increments the counter for cards that have queen as their identity by one
        break; //Breaks out of the condition
    } //End of switch statement
    
    //Second card:
    //Assigns the suit name 
    if (RandomNumber2 <= 13) { //If the boolean expression that says that the random number for card 2 is less than or equal to 13 is true:
      Suit2 = "Diamonds"; //Assigns the suit of card 2 to diamonds 
    } else if (14 <= RandomNumber2 && RandomNumber2 <= 26) { //If the boolean expression that says that the random number for card 2 is between 14 and 26 is true:
      Suit2 = "Clubs"; //Assigns the suit of card 2 to clubs
    } else if (27 <= RandomNumber2 && RandomNumber2 <= 39) { //If the boolean expression that says that the random number for card 2 is between 27 and 39 is true:
      Suit2 = "Hearts"; //Assigns the suit of card 2 to hearts
    } else if (40 <= RandomNumber2 && RandomNumber2 <= 52) { //If the boolean expression that says that the random number for card 2 is between 40 and 52 is true:
      Suit2 = "Spades"; //Assigns the suit of card 2 to spades
    } //End of if statement
    
    //Assigns the card identity
    int Remainder2 = RandomNumber2%13; //Calculates the remainder of the division between the random number for card 2 and 13 and declares it as an integer variable
    switch (Remainder2) { //Evaluates the value of the remainder for card 2
      case 0: //If the remainder for card 2 is 0:
        Identity2 = "King"; //Assigns the identity of card 2 to king
        King++; //Increments the counter for cards that have king as their identity by one
        break; //Breaks out of the condition 
      case 1: //If the remainder for card 2 is 1:
        Identity2 = "Ace"; //Assigns the identity of card 2 to ace
        Ace++; //Increments the counter for cards that have ace as their identity by one
        break; //Breaks out of the condition 
      case 2: //If the remainder for card 2 is 2:
        Identity2 = "2"; //Assigns the identity of card 2 to two
        Two++; //Increments the counter for cards that have two as their identity by one
        break; //Breaks out of the condition 
      case 3: //If the remainder for card 2 is 3:
        Identity2 = "3"; //Assigns the identity of card 2 to three
        Three++; //Increments the counter for cards that have three as their identity by one
        break; //Breaks out of the condition 
      case 4: //If the remainder for card 2 is 4:
        Identity2 = "4"; //Assigns the identity of card 2 to four
        Four++; //Increments the counter for cards that have four as their identity by one
        break; //Breaks out of the condition 
      case 5: //If the remainder for card 2 is 5:
        Identity2 = "5"; //Assigns the identity of card 2 to five
        Five++; //Increments the counter for cards that have five as their identity by one
        break; //Breaks out of the condition 
      case 6: //If the remainder for card 2 is 6:
        Identity2 = "6"; //Assigns the identity of card 2 to six
        Six++; //Increments the counter for cards that have six as their identity by one
        break; //Breaks out of the condition 
      case 7: //If the remainder for card 2 is 7:
        Identity2 = "7"; //Assigns the identity of card 2 to seven
        Seven++; //Increments the counter for cards that have seven as their identity by one
        break; //Breaks out of the condition 
      case 8: //If the remainder for card 2 is 8:
        Identity2 = "8"; //Assigns the identity of card 2 to eight
        Eight++; //Increments the counter for cards that have eight as their identity by one
        break; //Breaks out of the condition 
      case 9: //If the remainder for card 2 is 9:
        Identity2 = "9"; //Assigns the identity of card 2 to nine
        Nine++; //Increments the counter for cards that have nine as their identity by one
        break; //Breaks out of the condition 
      case 10: //If the remainder for card 2 is 10:
        Identity2 = "10"; //Assigns the identity of card 2 to ten
        Ten++; //Increments the counter for cards that have ten as their identity by one
        break; //Breaks out of the condition 
      case 11: //If the remainder for card 2 is 11:
        Identity2 = "Jack"; //Assigns the identity of card 2 to jack
        Jack++; //Increments the counter for cards that have jack as their identity by one
        break; //Breaks out of the condition 
      case 12: //If the remainder for card 2 is 12:
        Identity2 = "Queen"; //Assigns the identity of card 2 to queen
        Queen++; //Increments the counter for cards that have queen as their identity by one
        break; //Breaks out of the condition 
    } //End of switch statement
    
    //Third card: 
    //Assigns the suit name 
    if (RandomNumber3 <= 13) { //If the boolean expression that says that the random number for card 3 is less than or equal to 13 is true:
      Suit3 = "Diamonds"; //Assigns the suit of card 3 to diamonds
    } else if (14 <= RandomNumber3 && RandomNumber3 <= 26) { //If the boolean expression that says that the random number for card 3 is between 14 and 26 is true:
      Suit3 = "Clubs"; //Assigns the suit of card 3 to clubs
    } else if (27 <= RandomNumber3 && RandomNumber3 <= 39) { //If the boolean expression that says that the random number for card 3 is between 27 and 39 is true:
      Suit3 = "Hearts"; //Assigns the suit of card 3 to hearts
    } else if (40 <= RandomNumber3 && RandomNumber3 <= 52) { //If the boolean expression that says that the random number for card 3 is between 40 and 52 is true:
      Suit3 = "Spades"; //Assigns the suit of card 3 to spades
    } //End of if statement
    
    //Assigns the card identity
    int Remainder3 = RandomNumber3%13; //Calculates the remainder of the division between the random number for card 3 and 13 and declares it as an integer variable
    switch (Remainder3) { //Evaluates the value of the remainder for card 3
      case 0: //If the remainder for card 3 is 0:
        Identity3 = "King"; //Assigns the identity of card 3 to king
        King++; //Increments the counter for cards that have king as their identity by one
        break; //Breaks out of the condition 
      case 1: //If the remainder for card 3 is 1:
        Identity3 = "Ace"; //Assigns the identity of card 3 to ace
        Ace++; //Increments the counter for cards that have ace as their identity by one
        break; //Breaks out of the condition 
      case 2: //If the remainder for card 3 is 2:
        Identity3 = "2"; //Assigns the identity of card 3 to two
        Two++; //Increments the counter for cards that have two as their identity by one
        break; //Breaks out of the condition 
      case 3: //If the remainder for card 3 is 3:
        Identity3 = "3"; //Assigns the identity of card 3 to three
        Three++; //Increments the counter for cards that have three as their identity by one
        break; //Breaks out of the condition 
      case 4: //If the remainder for card 3 is 4:
        Identity3 = "4"; //Assigns the identity of card 3 to four
        Four++; //Increments the counter for cards that have four as their identity by one
        break; //Breaks out of the condition 
      case 5: //If the remainder for card 3 is 5:
        Identity3 = "5"; //Assigns the identity of card 3 to five
        Five++; //Increments the counter for cards that have five as their identity by one
        break; //Breaks out of the condition 
      case 6: //If the remainder for card 3 is 6:
        Identity3 = "6"; //Assigns the identity of card 3 to six
        Six++; //Increments the counter for cards that have six as their identity by one
        break; //Breaks out of the condition 
      case 7: //If the remainder for card 3 is 7:
        Identity3 = "7"; //Assigns the identity of card 3 to seven
        Seven++; //Increments the counter for cards that have seven as their identity by one
        break; //Breaks out of the condition 
      case 8: //If the remainder for card 3 is 8:
        Identity3 = "8"; //Assigns the identity of card 3 to eight
        Eight++; //Increments the counter for cards that have eight as their identity by one
        break; //Breaks out of the condition 
      case 9: //If the remainder for card 3 is 9:
        Identity3 = "9"; //Assigns the identity of card 3 to nine
        Nine++; //Increments the counter for cards that have nine as their identity by one
        break; //Breaks out of the condition 
      case 10: //If the remainder for card 3 is 10:
        Identity3 = "10"; //Assigns the identity of card 3 to ten
        Ten++; //Increments the counter for cards that have ten as their identity by one
        break; //Breaks out of the condition 
      case 11: //If the remainder for card 3 is 11:
        Identity3 = "Jack"; //Assigns the identity of card 3 to jack
        Jack++; //Increments the counter for cards that have jack as their identity by one
        break; //Breaks out of the condition 
      case 12: //If the remainder for card 3 is 12:
        Identity3 = "Queen"; //Assigns the identity of card 3 to queen
        Queen++; //Increments the counter for cards that have queen as their identity by one
        break; //Breaks out of the condition 
    } // End of switch statement
    
    //Forth card:
    //Assigns the suit name 
    if (RandomNumber4 <= 13) { //If the boolean expression that says that the random number for card 4 is less than or equal to 13 is true:
      Suit4 = "Diamonds"; //Assigns the suit of card 4 to diamonds
    } else if (14 <= RandomNumber4 && RandomNumber4 <= 26) { //If the boolean expression that says that the random number for card 4 is between 14 and 26 is true:
      Suit4 = "Clubs"; //Assigns the suit of card 4 to clubs
    } else if (27 <= RandomNumber4 && RandomNumber4 <= 39) { //If the boolean expression that says that the random number for card 4 is between 27 and 39 is true:
      Suit4 = "Hearts"; //Assigns the suit of card 4 to hearts
    } else if (40 <= RandomNumber4 && RandomNumber4 <= 52) { //If the boolean expression that says that the random number for card 4 is between 40 and 52 is true:
      Suit4 = "Spades"; //Assigns the suit of card 4 to spades
    } //End of if statement
    
    //Assigns the card identity
    int Remainder4 = RandomNumber4%13; //Calculates the remainder of the division between the random number for card 4 and 13 and declares it as an integer variable
    switch (Remainder4) { //Evaluates the value of the remainder for card 4
      case 0: //If the remainder for card 4 is 0:
        Identity4 = "King"; //Assigns the identity of card 4 to king
        King++; //Increments the counter for cards that have king as their identity by one
        break; //Breaks out of the condition 
      case 1: //If the remainder for card 4 is 1:
        Identity4 = "Ace"; //Assigns the identity of card 4 to ace
        Ace++; //Increments the counter for cards that have ace as their identity by one
        break; //Breaks out of the condition 
      case 2: //If the remainder for card 4 is 2:
        Identity4 = "2"; //Assigns the identity of card 4 to two
        Two++; //Increments the counter for cards that have two as their identity by one
        break; //Breaks out of the condition 
      case 3: //If the remainder for card 4 is 3:
        Identity4 = "3"; //Assigns the identity of card 4 to three
        Three++; //Increments the counter for cards that have three as their identity by one
        break; //Breaks out of the condition 
      case 4: //If the remainder for card 4 is 4:
        Identity4 = "4"; //Assigns the identity of card 4 to four
        Four++; //Increments the counter for cards that have four as their identity by one
        break; //Breaks out of the condition 
      case 5: //If the remainder for card 4 is 5:
        Identity4 = "5"; //Assigns the identity of card 4 to five
        Five++; //Increments the counter for cards that have five as their identity by one
        break; //Breaks out of the condition 
      case 6: //If the remainder for card 4 is 6:
        Identity4 = "6"; //Assigns the identity of card 4 to six
        Six++; //Increments the counter for cards that have six as their identity by one
        break; //Breaks out of the condition 
      case 7: //If the remainder for card 4 is 7:
        Identity4 = "7"; //Assigns the identity of card 4 to seven
        Seven++; //Increments the counter for cards that have seven as their identity by one
        break; //Breaks out of the condition 
      case 8: //If the remainder for card 4 is 8:
        Identity4 = "8"; //Assigns the identity of card 4 to eight
        Eight++; //Increments the counter for cards that have eight as their identity by one
        break; //Breaks out of the condition 
      case 9: //If the remainder for card 4 is 9:
        Identity4 = "9"; //Assigns the identity of card 4 to nine
        Nine++; //Increments the counter for cards that have nine as their identity by one
        break; //Breaks out of the condition 
      case 10: //If the remainder for card 4 is 10:
        Identity4 = "10"; //Assigns the identity of card 4 to ten
        Ten++; //Increments the counter for cards that have ten as their identity by one
        break; //Breaks out of the condition 
      case 11: //If the remainder for card 4 is 11:
        Identity4 = "Jack"; //Assigns the identity of card 4 to jack
        Jack++; //Increments the counter for cards that have jack as their identity by one
        break; //Breaks out of the condition 
      case 12: //If the remainder for card 4 is 12:
        Identity4 = "Queen"; //Assigns the identity of card 4 to queen
        Queen++; //Increments the counter for cards that have queen as their identity by one
        break; //Breaks out of the condition 
    } //End of switch statement
    
    //Fifth card:
    //Assigns the suit name 
    if (RandomNumber5 <= 13) { //If the boolean expression that says that the random number for card 5 is less than or equal to 13 is true:
      Suit5 = "Diamonds"; //Assigns the suit of card 5 to diamonds
    } else if (14 <= RandomNumber5 && RandomNumber5 <= 26) { //If the boolean expression that says that the random number for card 5 is between 14 and 26 is true:
      Suit5 = "Clubs"; //Assigns the suit of card 5 to clubs
    } else if (27 <= RandomNumber5 && RandomNumber5 <= 39) { //If the boolean expression that says that the random number for card 5 is between 27 and 39 is true:
      Suit5 = "Hearts"; //Assigns the suit of card 5 to hearts
    } else if (40 <= RandomNumber5 && RandomNumber5 <= 52) { //If the boolean expression that says that the random number for card 5 is between 40 and 52 is true:
      Suit5 = "Spades"; //Assigns the suit of card 5 to spades
    } //End of if statement
    
    //Assigns the card identity
    int Remainder5 = RandomNumber5%13; //Calculates the remainder of the division between the random number for card 5 and 13 and declares it as an integer variable
    switch (Remainder5) { //Evaluates the value of the remainder for card 5
      case 0: //If the remainder for card 5 is 0:
        Identity5 = "King"; //Assigns the identity of card 5 to king
        King++; //Increments the counter for cards that have king as their identity by one
        break; //Breaks out of the condition 
      case 1: //If the remainder for card 5 is 1:
        Identity5 = "Ace"; //Assigns the identity of card 5 to ace
        Ace++; //Increments the counter for cards that have ace as their identity by one
        break; //Breaks out of the condition 
      case 2: //If the remainder for card 5 is 2:
        Identity5 = "2"; //Assigns the identity of card 5 to two
        Two++; //Increments the counter for cards that have two as their identity by one
        break; //Breaks out of the condition 
      case 3: //If the remainder for card 5 is 3:
        Identity5 = "3"; //Assigns the identity of card 5 to three
        Three++; //Increments the counter for cards that have three as their identity by one
        break; //Breaks out of the condition 
      case 4: //If the remainder for card 5 is 4:
        Identity5 = "4"; //Assigns the identity of card 5 to four
        Four++; //Increments the counter for cards that have four as their identity by one
        break; //Breaks out of the condition 
      case 5: //If the remainder for card 5 is 5:
        Identity5 = "5"; //Assigns the identity of card 5 to five
        Five++; //Increments the counter for cards that have five as their identity by one
        break; //Breaks out of the condition 
      case 6: //If the remainder for card 5 is 6:
        Identity5 = "6"; //Assigns the identity of card 5 to six
        Six++; //Increments the counter for cards that have six as their identity by one
        break; //Breaks out of the condition 
      case 7: //If the remainder for card 5 is 7:
        Identity5 = "7"; //Assigns the identity of card 5 to seven
        Seven++; //Increments the counter for cards that have seven as their identity by one
        break; //Breaks out of the condition 
      case 8: //If the remainder for card 5 is 8:
        Identity5 = "8"; //Assigns the identity of card 5 to eight
        Eight++; //Increments the counter for cards that have eight as their identity by one
        break; //Breaks out of the condition 
      case 9: //If the remainder for card 5 is 9:
        Identity5 = "9"; //Assigns the identity of card 5 to nine
        Nine++; //Increments the counter for cards that have nine as their identity by one
        break; //Breaks out of the condition 
      case 10: //If the remainder for card 5 is 10:
        Identity5 = "10"; //Assigns the identity of card 5 to ten
        Ten++; //Increments the counter for cards that have ten as their identity by one
        break; //Breaks out of the condition 
      case 11: //If the remainder for card 5 is 11:
        Identity5 = "Jack"; //Assigns the identity of card 5 to jack
        Jack++; //Increments the counter for cards that have jack as their identity by one
        break; //Breaks out of the condition 
      case 12: //If the remainder for card 5 is 12:
        Identity5 = "Queen"; //Assigns the identity of card 5 to queen
        Queen++; //Increments the counter for cards that have queen as their identity by one
        break; //Breaks out of the condition 
    } //End of switch statement
    
    //Creates string variables for the cards 
    String Card1 = Identity1 + " of " + Suit1; //Declares and assigns the identity plus the suit of card 1 as a string variable 
    String Card2 = Identity2 + " of " + Suit2; //Declares and assigns the identity plus the suit of card 2 as a string variable
    String Card3 = Identity3 + " of " + Suit3; //Declares and assigns the identity plus the suit of card 3 as a string variable
    String Card4 = Identity4 + " of " + Suit4; //Declares and assigns the identity plus the suit of card 4 as a string variable
    String Card5 = Identity5 + " of " + Suit5; //Declares and assigns the identity plus the suit of card 5 as a string variable
    
    //Print out output data 
    System.out.println("Your random cards were:"); //Print out the statement to the terminal window 
    System.out.println("The " + Card1); //Print out the card 1 value to the terminal window
    System.out.println("The " + Card2); //Print out the card 2 value to the terminal window
    System.out.println("The " + Card3); //Print out the card 3 value to the terminal window
    System.out.println("The " + Card4); //Print out the card 4 value to the terminal window
    System.out.println("The " + Card5); //Print out the card 5 value to the terminal window

    switch (Ace) { //Evalutes the counter for cards that have ace as their identity
      case 2: //If the number of the cards that have ace as their identity is 2:
        if ((Ace == Two) || (Ace == Three) || (Ace == Four) || (Ace == Five) || (Ace == Six) || (Ace == Seven) || (Ace == Eight) || (Ace == Nine) || (Ace == Ten) || (Ace == Jack) || (Ace == Queen) || (Ace == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else { //Otherwise:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition  
      case 3: //If the number of the cards that have ace as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
      
    switch (Two) { //Evalutes the counter for cards that have two as their identity
      case 2: //If the number of the cards that have two as their identity is 2:
        if ((Two == Three) || (Two == Four) || (Two == Five) || (Two == Six) || (Two == Seven) || (Two == Eight) || (Two == Nine) || (Two == Ten) || (Two == Jack) || (Two == Queen) || (Two == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if (Two != Ace) { //If the boolean expression is true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition
      case 3: //If the number of the cards that have two as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
    } //End of switch statement
    
    switch (Three) { //Evalutes the counter for cards that have three as their identity
      case 2: //If the number of the cards that have three as their identity is 2:
        if ((Three == Four) || (Three == Five) || (Three == Six) || (Three == Seven) || (Three == Eight) || (Three == Nine) || (Three == Ten) || (Three == Jack) || (Three == Queen) || (Three == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Three != Two) && (Three != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have three as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Four) { //Evalutes the counter for cards that have four as their identity
      case 2: //If the number of the cards that have four as their identity is 2:
        if ((Four == Five) || (Four == Six) || (Four == Seven) || (Four == Eight) || (Four == Nine) || (Four == Ten) || (Four == Jack) || (Four == Queen) || (Four == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Four != Three) && (Four != Two) && (Four != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have four as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Five) { //Evalutes the counter for cards that have five as their identity
      case 2: //If the number of the cards that have five as their identity is 2:
        if ((Five == Six) || (Five == Seven) || (Five == Eight) || (Five == Nine) || (Five == Ten) || (Five == Jack) || (Five == Queen) || (Five == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Five != Four) && (Five != Three) && (Five != Two) && (Five != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have five as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Six) { //Evalutes the counter for cards that have six as their identity
      case 2: //If the number of the cards that have six as their identity is 2:
        if ((Six == Seven) || (Six == Eight) || (Six == Nine) || (Six == Ten) || (Six == Jack) || (Six == Queen) || (Six == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Six != Five) && (Six != Four) && (Six != Three) && (Six != Two) && (Six != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have six as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Seven) { //Evalutes the counter for cards that have seven as their identity
      case 2: //If the number of the cards that have seven as their identity is 2:
        if ((Seven == Eight) || (Seven == Nine) || (Seven == Ten) || (Seven == Jack) || (Seven == Queen) || (Seven == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Seven != Six) && (Seven != Five) && (Seven != Four) && (Seven != Three) && (Seven != Two) && (Seven != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have seven as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Eight) { //Evalutes the counter for cards that have eight as their identity
      case 2: //If the number of the cards that have eight as their identity is 2:
        if ((Eight == Nine) || (Eight == Ten) || (Eight == Jack) || (Eight == Queen) || (Eight == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Eight != Seven) && (Eight != Six) && (Eight != Five) && (Eight != Four) && (Eight != Three) && (Eight != Two) && (Eight != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have eight as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Nine) { //Evalutes the counter for cards that have nine as their identity
      case 2: //If the number of the cards that have nine as their identity is 2:
        if ((Nine == Ten) || (Nine == Jack) || (Nine == Queen) || (Nine == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Nine != Eight) && (Nine != Seven) && (Nine != Six) && (Nine != Five) && (Nine != Four) && (Nine != Three) && (Nine != Two) && (Nine != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have nine as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Ten) { //Evalutes the counter for cards that have ten as their identity
      case 2: //If the number of the cards that have ten as their identity is 2:
        if ((Ten == Jack) || (Ten == Queen) || (Ten == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Ten != Nine) && (Ten != Eight) && (Ten != Seven) && (Ten != Six) && (Ten != Five) && (Ten != Four) && (Ten != Three) && (Ten != Two) && (Ten != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have ten as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Jack) { //Evalutes the counter for cards that have jack as their identity
      case 2: //If the number of the cards that have jack as their identity is 2:
        if ((Jack == Queen) || (Jack == King)) { //If any the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Jack != Ten) && (Jack != Nine) && (Jack != Eight) && (Jack != Seven) && (Jack != Six) && (Jack != Five) && (Jack != Four) && (Jack != Three) && (Jack != Two) && (Jack != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have jack as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (Queen) { //Evalutes the counter for cards that have queen as their identity
      case 2: //If the number of the cards that have queen as their identity is 2:
        if (Queen == King) { //If the boolean expressions is true:
          System.out.println("You have two pairs!"); //Prints out the statement to the terminal window
        } else if ((Queen != Jack) && (Queen != Ten) && (Queen != Nine) && (Queen != Eight) && (Queen != Seven) && (Queen != Six) && (Queen != Five) && (Queen != Four) && (Queen != Three) && (Queen != Two) && (Queen != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have queen as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    switch (King) { //Evalutes the counter for cards that have king as their identity
      case 2: //If the number of the cards that have king as their identity is 2:
        if ((King != Queen) && (King != Jack) && (King != Ten) && (King != Nine) && (King != Eight) && (King != Seven) && (King != Six) && (King != Five) && (King != Four) && (King != Three) && (King != Two) && (King != Ace)) { //If the boolean expressions are true:
          System.out.println("You have a pair!"); //Prints out the statement to the terminal window
        } //End of if statement
        break; //Breaks out of the condition 
      case 3: //If the number of the cards that have king as their identity is 3:
        System.out.println("You have a three of a kind!"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition 
    } //End of switch statement
    
    if ((Identity1 != Identity2) && (Identity1 != Identity3) && (Identity1 != Identity4) && (Identity1 != Identity5) && (Identity2 != Identity3) && (Identity2 != Identity4) && (Identity2 != Identity5) && (Identity3 != Identity4) && (Identity3 != Identity5) && (Identity4 != Identity5)) { //If the boolean expressions are true:
      System.out.println("You have a high card hand!"); //Prints out the statement to the terminal window
    } //End of if statement
    
  } //End of main method
  
} //End of class