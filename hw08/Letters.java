//////////////
//// CSE2 Letters 
//// 04/09/19 Rafaela Mantoan Borges 
//// This program should: 
// Create an array with an arbitrary size and filled with random chars that are lowercase and uppercase letters
// Separate the first array into two different arrays using two methods

import java.util.Arrays; //Imports Arrays class 
import java.util.Random; //Imports Random class 

public class Letters {
  //Main method:
  public static void main (String[] args) { //Main method required by every Java program
    Random RandomGenerator = new Random(); //Declares and constructs an instance
    int RandomInt = RandomGenerator.nextInt(49 + 1) + 1; //Generates a random integer between 1 and 50 (inclusive)
    char[] Array = new char[RandomInt]; //Declares and allocates space for the array 
    System.out.print("Random character array: "); //Prints out the statement to the terminal window
    for (int i = 0; i < RandomInt; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the random integer
      int RandomNumber = RandomGenerator.nextInt((122-65) + 1) + 65; //Generates an integer between 65 and 122 (inclusive)
      do { //Does this while the number is between 91 and 96 (inclusive):
        if (RandomNumber >= 91 && RandomNumber <= 96) { //If the random integer is between 91 and 96 (if the random integer doesn't correspond to a letter on the ACII table):
          RandomNumber = RandomGenerator.nextInt((122-65) + 1) + 65; //Generate another integer between 65 and 122 (inclusive)
        } //End of if statement
      } while (RandomNumber >= 91 && RandomNumber <= 96); //End of do-while loop
      Array[i] = (char) (RandomNumber); //Initializes the ith element of the array by converting the integer into a character 
    } //End of for loop
    System.out.println(Array); //Prints out the array to the terminal window
    char[] AtoM = getAtoM(Array); //Calls method getAtoM and stores the modified array
    System.out.print("AtoM characters: "); //Prints out the statement to the terminal window
    System.out.println(AtoM); //Prints out modified array to terminal window
    char[] NtoZ = getNtoZ(Array); //Calls method getNtoZ and stores the modified array
    System.out.print("NtoZ characters: "); //Prints out the statement to the terminal window
    System.out.println(NtoZ); //Prints out modified array to terminal window
  } //End of main method 
  
  //Method to find all the upper and lowercase letters from A to M (inclusive):
  public static char[] getAtoM (char[] Array) { //This method accepts an array of characters as input and returns an array of characters 
    int Counter = 0; //Declares and initializes a counter 
    for (int i = 0; i < Array.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the array  
      if ((Array[i] >= 'A' && Array[i] <= 'M') || (Array[i] >= 'a' && Array[i] <= 'm')) { //If the ith element of the array is between A and M or between a and M:
       Counter++; //Increments counter by 1 when the element if between the desired range 
      } //End of if statement 
    } //End of for loop
    char[] AtoM = new char[Counter]; //Declares and allocates space for the array 
    int i = 0; //Declares and initializes a counter
    for (int j = 0; j < Counter; j++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the number of elements between the desired range 
      for (i = i; i < Array.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the new array
        if ((Array[i] >= 'A' && Array[i] <= 'M') || (Array[i] >= 'a' && Array[i] <= 'm')) { //If the ith element of the array is between A and M or between a and M:
          AtoM[j] = Array[i]; //Assigns the jth element of the new array to the ith element of the original array 
          break; //Breaks out of the loop 
        } //End of if statement  
      } //End of for loop 
      i++; //Increments the counter by 1 so the previous for loop can iniciate with the updated i value 
    } //End of for loop 
    return AtoM; //Returns the new array 
  } //End of getAtoM method 
  
  //Method to find all the upper and lowercase letters from N to Z (inclusive):
  public static char[] getNtoZ (char[] Array) { //This method accepts an array of characters as input and returns an array of characters 
    int Counter = 0; //Declares and initializes a counter 
    for (int i = 0; i < Array.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the array
      if ((Array[i] >= 'N' && Array[i] <= 'Z') || (Array[i] >= 'n' && Array[i] <= 'z')) { //If the ith element of the array is between N and Z or between n and z:
       Counter++; //Increments counter by 1 when the element if between the desired range 
      } //End of if statement 
    } //End of for loop
    char[] NtoZ = new char[Counter]; //Declares and allocates space for the array 
    int i = 0; //Declares and initializes a counter
    for (int j = 0; j < Counter; j++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the number of elements between the desired range 
      for (i = i; i < Array.length; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the length of the new array
        if ((Array[i] >= 'N' && Array[i] <= 'Z') || (Array[i] >= 'n' && Array[i] <= 'z')) { //If the ith element of the array is between N and Z or between n and z:
          NtoZ[j] = Array[i]; //Assigns the jth element of the new array to the ith element of the original array 
          break; //Breaks out of the loop 
        } //End of if statement  
      } //End of for loop
      i++; //Increments the counter by 1 so the previous for loop can iniciate with the updated i value 
    } //End of for loop 
    return NtoZ; //Returns the new array 
  } //End of getAtoM method 
  
} //End of class