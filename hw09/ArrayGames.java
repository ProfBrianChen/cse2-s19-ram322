//////////////
//// CSE2 Array Games 
//// 04/16/19 Rafaela Mantoan Borges
//// This program should: 
// Generate integer arrays of a random size between 10 and 20 members.
// Print out the members of an integer array
// Produce a new array long enough to contain two arrays in a specific way (Method Insert)
// Produce a new array having length that is one member less than the original array if the input integer actually refers to an index within the range of the array’s length (Method Shorten) 

import java.util.Arrays; //import Arrays class
import java.util.Random; //import Random class
import java.util.Scanner; //import Scanner class

public class ArrayGames { 
  
  //Method to generate arrays:
  public static int[] Generate () { //This method returns an array of integers  
    Random RandomGenerator = new Random(); //Declares and constructs an instance
    int RandomInt = RandomGenerator.nextInt((20-10) + 1) + 10; //Generates an integer between 10 and 20 (inclusive)
    int[] Array = new int[RandomInt]; //Declares and allocates space for an array of length equal to the random integer generated 
    for (int i = 0; i < RandomInt; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the random integer
      Array[i] = RandomGenerator.nextInt((20-10) + 1) + 10; //Generates integers between 10 and 20 (inclusive) to be elements of the array
    } //End of for loop 
    return Array; //Return array 
  } //End of Generate method   
  
  //Method to print arrays: 
  public static void Print (int[] Array) { //This method accepts an integer array 
    System.out.print("{"); //Prints out the bracket to terminal window
    for (int i = 0; i < Array.length; i++) { //Declares and initializes a counter incrementing it by one until it is equal to the length of the array
      if (i == (Array.length-1)) { //If the counter is equal to the length of the array minus one (last element):
        System.out.print(Array[i] + "}"); //Prints out the ith elemet of the array and the bracket 
      } else { //Else:
        System.out.print(Array[i] + ", "); //Prints out the ith elemet of the array
      } //End of if statement       
    } //End of for loop 
    System.out.println(); //Returns to the next line 
  } //End of Print method 
  
  //Method to produce a new array long enough to contain two arrays in a specific way:
  public static int[] Insert (int[] Array1, int[] Array2) { //This method accepts two integer arrays and returns an integer array 
    Random RandomGenerator = new Random(); //Declares and constructs an instance
    int[] Array = new int[Array1.length+Array2.length]; //Declares and allocates space for an array with length of the two arrays combined
    int RandomInt = RandomGenerator.nextInt((Array1.length-1) + 1); //Generates a random integer within the range of the array’s length
    System.out.println("Input 2 should start at Output[" + RandomInt + "]"); //Prints out the statement to the terminal window 
    int j = 0; //Declares and initializes a counter 
    int k = RandomInt; //Declares and initializes another counter
    for (int i = 0; i < RandomInt; i++) { //Declares and initializes a counter incrementing it by one until it is equal to the random integer 
      Array[i] = Array1[i]; //Inserts members of the first array up until the ith elemet
    } //End of for loop 
    for (int i = RandomInt; i < (Array2.length+RandomInt); i++) { //Declares and initializes a counter incrementing it by one until it is equal to the length of the second array plus the random integer
      Array[i] = Array2[j]; //Inserts every member of the second array 
      j++; //Increments counter by one 
    } //End of for loop 
    if (Array1.length+Array2.length > Array2.length+RandomInt) { //If the first array still has members left: 
      for (int i = (Array2.length+RandomInt); i < (Array1.length+Array2.length); i++) { //Declares and initializes a counter incrementing it by one until it is equal to the length of the new array
        Array[i] = Array1[k]; //Inserts the rest of the members of the first array 
        k++; //Increments counter by one 
      } //End of for loop 
    } //End of if statement
    return Array; //Returns new array 
  } //End of Insert method
  
  //Method to produce a new array having length that is one member less than the original array if the integer input has the right value: 
  public static int[] Shorten (int[] Array, int Value) { //This method accepts an integer array and an integer and returns an array of integers
    boolean Condition = false; //Declares and initiaizes a boolean
    int Int = 0; //Declares and initializes an integer
    for (int i = 0; i < Array.length; i++) { //Declares and initializes a counter incrementing it by one until it is equal to the length of the array
      if (i == Value) { //If the counter is equal to the integer input:
        Condition = true; //Assigns the boolean to true 
        Int = i; //Assigns the integer to the counter
        break; //Breaks out of the loop 
      } //End of if statement
    } //End of for loop 
    if (Condition) { //If boolean is true:
      int[] ModifiedArray = new int[Array.length-1]; //Declares and allocates space for a new array of length that is one member less than the original array
      for (int i = 0; i < Int; i++) { //Declares and initializes a counter incrementing it by one until it is equal to the integer
        ModifiedArray[i] = Array[i]; //Inserts members of the first array up until the ith elemet
      } //End of for loop 
      for (int i = Int; i < (Array.length-1); i++) { //Declares and initializes a counter incrementing it by one until it is equal to length of the array minus one 
        ModifiedArray[i] = Array[i+1]; //Removes the member referenced by the integer input index
      } //End of for loop 
      return ModifiedArray; //Returns the new array 
    } else { //If boolean is false:
      return Array; //Returns original array 
    } //End of if statement
  } //End of Shorten method 
  
  //Main method 
  public static void main (String[] args) { //Main method required for every Java program 
    Scanner myScanner = new Scanner(System.in); //Declares and constructs an instance for myScanner
    System.out.print("Would you like to run Insert or Shorten? "); //Prompts the user for the method type 
    String Answer = myScanner.next(); //Accepts and returns the string 
    do { //Does while the method type is different from the acceptable ones:
      if (!(Answer.equals("Insert")) && !(Answer.equals("Shorten"))) { //If the method type is different from the acceptable ones:
        System.out.println("ERROR: please reply Insert or Shorten only"); //Prints out error message to the terminal window
        System.out.print("Would you like to run Insert or Shorten? "); //Prompts the user for the method type 
        Answer = myScanner.next(); //Accepts and returns the string 
      } //End of if statement 
    } while (!(Answer.equals("Insert")) && !(Answer.equals("Shorten"))); //End of do-while loop
    if (Answer.equals("Insert")) { //If answer equals Insert:
      int[] Input1 = Generate(); //Calls Generate method 
      System.out.print("Input 1: "); //Prints out the statement to terminal window
      Print(Input1); //Calls Print method to print Input1
      int[] Input2 = Generate(); //Calls Generate method 
      System.out.print("Input 2: "); //Prints out the statement to terminal window
      Print(Input2); //Calls Print method to print Input2
      int[] Output = Insert(Input1, Input2); //Calls Insert method using Input1 and Input2 as arguments 
      System.out.print("Output: "); //Prints out the statement to terminal window
      Print(Output); //Calls Print method to print Output 
    } else if (Answer.equals("Shorten")) { //If the answer equals Shorten:
      Random RandomGenerator = new Random(); //Declares and constructs an instance
      int[] Input1 = Generate(); //Calls Generate method 
      System.out.print("Input 1: "); //Prints out the statement to terminal window
      Print(Input1); //Calls Print method to print Input1
      int Input2 = RandomGenerator.nextInt(50 + 1); //Generates an integer between 0 and 50 (inclusive)
      System.out.println("Input 2: " + Input2); //Prints out Input2 to terminal window
      int[] Output = Shorten(Input1, Input2); //Calls Shorten method
      System.out.print("Output: "); //Prints out the statement to terminal window
      Print(Output); //Calls Print method to print Output 
    } //End of if statement
  } //End of main method 
  
} //End of class