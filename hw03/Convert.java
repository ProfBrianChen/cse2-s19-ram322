//////////////
//// CSE 02 Convert 
//// 02.12.19 Rafaela Mantoan Borges
//// Given the fact that the user wants to convert meters to inches, which should be stored in doubles, this program should:
// Prompt the user for a measurement in meters
// Calculate and print out the correct number of inches

import java.util.Scanner; //Imports Scanner class (import statement)

public class Convert {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner( System.in ); //Declares and constructs an instance
    
    //Input data
    System.out.print("Enter the distance in meters: "); //Prompts the user for the distance in meters
    double DistanceMeters = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called DistanceMeters
    
    //Output data
    double InchesPerMeter = 39.37007887817704; //Declares and assigns in/m convertion value to a double variable
    double DistanceInches = DistanceMeters*InchesPerMeter; //Converts the distance from meters to inches and stores the result in a double variable called DistanceInches
    
    //Prints out output data
    System.out.println(DistanceMeters + " meters is " + String.format("%.4f", DistanceInches) + " inches."); //Prints out the distance in inches 
    
  } //End of main method
  
} //End of class