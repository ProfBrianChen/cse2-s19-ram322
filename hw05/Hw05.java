//////////////
//// CSE2 Homework 05 
//// 03.06.19 Rafaela Mantoan Borges
//// Given that you are currently taking a course, this program should:
// Use the Scanner class to obtain from the user the course name
// Use the Scanner class to obtain from the user the department name
// Use the Scanner class to obtain from the user the number of meetings per week
// Use the Scanner class to obtain from the user the time the class starts
// Use the Scanner class to obtain from the user the name of the instructor 
// Use the Scanner class to obtain from the user the number of students 
// Check to make sure that what the user enters is actually of the correct type for each item and if not, use an infinite loop to ask again


import java.util.Scanner; //Imports Scanner class (import statement)

public class Hw05 {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner1 = new Scanner(System.in); //Declares and constructs an instance for myScanner1
    Scanner myScanner2 = new Scanner(System.in); //Declares and constructs an instance for myScanner2
    Scanner myScanner3 = new Scanner(System.in); //Declares and constructs an instance for myScanner3
    Scanner myScanner4 = new Scanner(System.in); //Declares and constructs an instance for myScanner4
    Scanner myScanner5 = new Scanner(System.in); //Declares and constructs an instance for myScanner5
    Scanner myScanner6 = new Scanner(System.in); //Declares and constructs an instance for myScanner6

    
    //Input data
    
    //Course number
    System.out.print("Course number: "); //Prompts the user for the course number
    boolean Condition1 = myScanner1.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int CourseNumber = 0; //Declares and assigns the course number to an integer  
    
    if (Condition1) { //If input is available to be read (if the condition is true):
      CourseNumber = myScanner1.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the course number is less than or equal to 0:
      if (Condition1) { //If input is available to be read (if the condition is true):
        if (CourseNumber <= 0) { //If the course number is less than or equal to 0:
          System.out.println("Error: positive integer needed"); //Prints out the error message to the terminal window 
          System.out.print("Course number: "); //Prompts the user for the course number
          Condition1 = myScanner1.hasNextInt(); //Checks if the input is available to be read
          if (Condition1) { //If input is available to be read (if the condition is true):
            CourseNumber = myScanner1.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("Error: input of integer type needed"); //Prints out the error message to the terminal window
        String junkWord1 = myScanner1.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Course number: "); //Prompts the user for the course number
        Condition1 = myScanner1.hasNextInt(); //Checks if the input is available to be read
        if (Condition1) { //If input is available to be read (if the condition is true):
          CourseNumber = myScanner1.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition1 == false) || (CourseNumber <= 0)); //End of do-while loop
    
    //Department name
    System.out.print("Department name: "); //Prompts the user for the department name 
    boolean Condition2 = myScanner2.hasNext(); //Declares and assigns a boolean to check if the input is available to be read
    String DepartmentName = "";  //Declares and assigns the department to a string 
    
    if (Condition2) { //If input is available to be read (if the condition is true):
      DepartmentName = myScanner2.next(); //Accepts and returns the string value
    } else { //If the condition is false (input is not available to be read):
      while (Condition2 == false) { //While the condition is false (input is not available to be read):
        System.out.println("Error: input of spring type needed"); //Prints out the error message to the terminal window 
        String junkWord2 = myScanner2.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Department name: "); //Prompts the user for the department name 
        Condition2 = myScanner2.hasNext(); //Checks if the input is available to be read
        if (Condition2) { //If input is available to be read (if the condition is true):
          DepartmentName = myScanner2.next(); //Accepts and returns the string value
        } //End of if statement 
      } //End of while loop       
    } //End of if statement
    
    //Number of meetings per week
    System.out.print("Number of meetings per week: "); //Prompts the user for the number of meetings per week
    boolean Condition3 = myScanner3.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int MeetingsPerWeek = 0; //Declares and assigns the number of meetings per week to an integer 
    
    if (Condition3) { //If input is available to be read (if the condition is true):
      MeetingsPerWeek = myScanner3.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the course number is less than or equal to 0:
      if (Condition3) { //If input is available to be read (if the condition is true):
        if (MeetingsPerWeek <= 0) { //If the number of meetings per week is less than or equal to 0:
          System.out.println("Error: positive integer needed"); //Prints out the error message to the terminal window 
          System.out.print("Number of meetings per week: "); //Prompts the user for the number of meetings per week
          Condition3 = myScanner3.hasNextInt(); //Checks if the input is available to be read
          if (Condition3) { //If input is available to be read (if the condition is true):
            MeetingsPerWeek = myScanner3.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("Error: input of integer type needed"); //Prints out the error message to the terminal window 
        String junkWord3 = myScanner3.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Number of meetings per week: "); //Prompts the user for the number of meetings per week
        Condition3 = myScanner3.hasNextInt(); //Checks if the input is available to be read
        if (Condition3) { //If input is available to be read (if the condition is true):
          MeetingsPerWeek = myScanner3.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement
    } while ((Condition3 == false) || (MeetingsPerWeek <= 0)); //End of do-while loop
    
    //Time the class starts 
    System.out.print("Time the class starts (hh:mm:ss): "); //Prompts the user for the time the class starts 
    boolean Condition4 = myScanner4.hasNext(); //Declares and assigns a boolean to check if the input is available to be read
    String StartTime = ""; //Declares and assigns the time the class starts to a string 
    
    if (Condition4) { //If input is available to be read (if the condition is true):
      StartTime = myScanner4.next(); //Accepts and returns the string value
    } else { //If the condition is false (input is not available to be read):
      while (Condition4 == false) { //While the condition is false (input is not available to be read):
        System.out.println("Error: input of spring type needed"); //Prints out the error message to the terminal window 
        String junkWord4 = myScanner4.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Time the class starts (hh:mm:ss): "); //Prompts the user for the time the class starts 
        Condition4 = myScanner4.hasNext(); //Checks if the input is available to be read
        if (Condition4) { //If input is available to be read (if the condition is true):
          StartTime = myScanner4.next(); //Accepts and returns the string value
        } //End of the if statement
      } //End of while loop        
    } //End of if statement
    
    //Name of the instructor
    System.out.print("Name of the instructor: "); //Prompts the user for the name of the instructor   
    boolean Condition5 = myScanner5.hasNext(); //Declares and assigns a boolean to check if the input is available to be read
    String Instructor = ""; //Declares and assigns the name of the instructor to a string 
    
    if (Condition5) { //If input is available to be read (if the condition is true):
      Instructor = myScanner5.next(); //Accepts and returns the string value
    } else { //If the condition is false (input is not available to be read):
      while (Condition5 == false) { //While the condition is false (input is not available to be read):
        System.out.println("Error: input of spring type needed"); //Prints out the error message to the terminal window 
        String junkWord5 = myScanner5.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Name of the instructor: "); //Prompts the user for the name of the instructor   
        Condition5 = myScanner5.hasNext(); //Checks if the input is available to be read
        if (Condition5) { //If input is available to be read (if the condition is true):
          Instructor = myScanner5.next(); //Accepts and returns the string value
        } //End of if statement
      } //End of while loop       
    } //End of if statement
    
    //Number of students
    System.out.print("Number of students: "); //Prompts the user for the number of students
    boolean Condition6 = myScanner6.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int Students = 0; //Declares and assigns the number of students to an integer 
    
    if (Condition6) { //If input is available to be read (if the condition is true):
      Students = myScanner6.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the course number is less than or equal to 0:
      if (Condition6) { //If input is available to be read (if the condition is true):
        if (Students <= 0) { //If the number of students is less than or equal to 0:
          System.out.println("Error: positive integer needed"); //Prints out the error message to the terminal window 
          System.out.print("Number of students: "); //Prompts the user for the number of students
          Condition6 = myScanner6.hasNextInt(); //Checks if the input is available to be read
          if (Condition6) { //If input is available to be read (if the condition is true):
            Students = myScanner6.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("Error: input of integer type needed"); //Prints out the error message to the terminal window 
        String junkWord6 = myScanner6.next(); //Declares and assigns a string to remove what was typed by the user
        System.out.print("Number of students: "); //Prompts the user for the number of students
        Condition6 = myScanner6.hasNextInt(); //Checks if the input is available to be read
        if (Condition6) { //If input is available to be read (if the condition is true):
            Students = myScanner6.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } while ((Condition6 == false) || (Students <= 0)); //End of do-while loop
    
  } //End of main method
  
} //End of class