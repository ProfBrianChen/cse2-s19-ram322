//////////////
//// CSE2 Network 
//// 03.19.19 Rafaela Mantoan Borges
//// This program should: 
// Query the user for the width and height of the display window
// Query the user for the size of the boxes and the length of an edge
// Work with any positive value for the size of the squares, the length of the edges, and the height and width of the display window
// Connect squares horizontally and vertically with an edge in the middle of the box
// Display a window into a “network” of boxes connected by lines 

import java.util.Scanner; //Imports Scanner class (import statement)

public class Network {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner( System.in ); //Declares and constructs an instance for myScanner1
    
    //Width of the display window
    System.out.print("Enter a positive integer to be the width of the display window: "); //Prompts the user for the width of the display window
    boolean Condition1 = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int Width = 0; //Declares and assigns the width of the display window to an integer  
    
    if (Condition1) { //If input is available to be read (if the condition is true):
      Width = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the width is less than or equal to 0:
      if (Condition1) { //If input is available to be read (if the condition is true):
        if (Width <= 0) { //If the width is less than or equal to 0:
          System.out.println("ERROR: POSITIVE INTEGER NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter a positive integer to be the width of the display window: "); //Prompts the user for the width of the display window
          Condition1 = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition1) { //If input is available to be read (if the condition is true):
            Width = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF INTEGER TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord1 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a positive integer to be the width of the display window: "); //Prompts the user for the width of the display window
        Condition1 = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition1) { //If input is available to be read (if the condition is true):
          Width = myScanner.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition1 == false) || (Width <= 0)); //End of do-while loop
    
    //Height of the display window
    System.out.print("Enter a positive integer to be the height of the display window: "); //Prompts the user for the height of the display window
    boolean Condition2 = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int Height = 0; //Declares and assigns the height of the display window to an integer  
    
    if (Condition2) { //If input is available to be read (if the condition is true):
      Height = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the height is less than or equal to 0:
      if (Condition2) { //If input is available to be read (if the condition is true):
        if (Height <= 0) { //If the height is less than or equal to 0:
          System.out.println("ERROR: POSITIVE INTEGER NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter a positive integer to be the height of the display window: "); //Prompts the user for the height of the display window
          Condition2 = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition2) { //If input is available to be read (if the condition is true):
            Height = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF INTEGER TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord2 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a positive integer to be the height of the display window: "); //Prompts the user for the height of the display window
        Condition2 = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition2) { //If input is available to be read (if the condition is true):
          Height = myScanner.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition2 == false) || (Height <= 0)); //End of do-while loop
    
    //Size of the boxes
    System.out.print("Enter a positive integer to be the size of the boxes: "); //Prompts the user for the size of the boxes
    boolean Condition3 = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int SizeOfBoxes = 0; //Declares and assigns the size of the boxes to an integer  
    
    if (Condition3) { //If input is available to be read (if the condition is true):
      SizeOfBoxes = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the size is less than or equal to 0:
      if (Condition3) { //If input is available to be read (if the condition is true):
        if (SizeOfBoxes <= 0) { //If the size is less than or equal to 0:
          System.out.println("ERROR: POSITIVE INTEGER NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter a positive integer to be the size of the boxes: "); //Prompts the user for the size of the boxes
          Condition3 = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition3) { //If input is available to be read (if the condition is true):
            SizeOfBoxes = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF INTEGER TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord3 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a positive integer to be the size of the boxes: "); //Prompts the user for the size of the boxes
        Condition3 = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition3) { //If input is available to be read (if the condition is true):
          SizeOfBoxes = myScanner.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition3 == false) || (SizeOfBoxes <= 0)); //End of do-while loop
    
    //Length of an edge
    System.out.print("Enter a positive integer to be the length of an edge: "); //Prompts the user for the length of an edge
    boolean Condition4 = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int LengthOfEdge = 0; //Declares and assigns the length of an edge to an integer  
    
    if (Condition4) { //If input is available to be read (if the condition is true):
      LengthOfEdge = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the length is less than or equal to 0:
      if (Condition4) { //If input is available to be read (if the condition is true):
        if (LengthOfEdge <= 0) { //If the length is less than or equal to 0:
          System.out.println("ERROR: POSITIVE INTEGER NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter a positive integer to be the length of an edge: "); //Prompts the user for the length of an edge
          Condition4 = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition4) { //If input is available to be read (if the condition is true):
            LengthOfEdge = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF INTEGER TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord4 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a positive integer to be the length of an edge: "); //Prompts the user for the length of an edge
        Condition4 = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition4) { //If input is available to be read (if the condition is true):
          LengthOfEdge = myScanner.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition4 == false) || (LengthOfEdge <= 0)); //End of do-while loop   
    
    //Prints out the network 
    int Remainder = SizeOfBoxes%2; //Calculates the remainder of the division and declares it as an integer 
    if (Remainder == 0) { //Even squares:
      if ((SizeOfBoxes <= Width) && (SizeOfBoxes <= Height)) { //If the size of the boxes is less than or equal to the display window 
      for (int Counter1 = 1; Counter1 <= (Height/(SizeOfBoxes+LengthOfEdge)); Counter1++) {  
        //First line:
        for (int Counter2 = 1; Counter2 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter2++) {  
          System.out.print("#"); 
          for (int Counter3 = 1; Counter3 <= (SizeOfBoxes-2); Counter3++) { 
            System.out.print("-"); 
          } 
          System.out.print("#"); 
          if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) { 
              System.out.print(" "); 
            } 
          } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:  
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) { 
              System.out.print("-"); 
            } 
          } 
        } 
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
          int Temp;
          System.out.print("#");
          if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
            for (Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
              System.out.print("-");
            }
          } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
            for (Temp = 1; Temp <= (SizeOfBoxes-2); Temp++) {
              System.out.print("-");
            }
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)) > 0) {
              if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
                for (int Temp2 = 0; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                  System.out.print(" ");
                }
              } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
                for (int Temp2 = 0; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                  System.out.print("-");
                }
              }
            }
          }
        }
        System.out.println();
        //Second line:
        for (int Counter5 = 1; Counter5 <= (SizeOfBoxes-2); Counter5++) {
          for (int Counter6 = 1; Counter6 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter6++) {
          System.out.print("|");
          for (int Counter7 = 1; Counter7 <= (SizeOfBoxes-2); Counter7++) {
            System.out.print(" ");
          }
          System.out.print("|");
          if (Counter5 == ((SizeOfBoxes-2)/2) || Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
            for (int Counter8 = 1; Counter8 <= LengthOfEdge; Counter8++) {
              System.out.print("-");
            }
          } else {
            for (int Counter9 = 1; Counter9 <= LengthOfEdge; Counter9++) {
              System.out.print(" ");
              }
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
              int Temp3;
              System.out.print("|");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                for (Temp3 = 1; Temp3 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp3++) {
                  System.out.print(" ");
                }
              } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                for (Temp3 = 1; Temp3 <= (SizeOfBoxes-2); Temp3++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)) > 0) {
                  if (Counter5 == ((SizeOfBoxes-2)/2) || Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
                    for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                      System.out.print("-");
                    }
                  } else {
                    for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                      System.out.print(" ");
                    }
                  }
                }
              }
            }
          System.out.println();
        }
        //Third line:
        for (int Counter10 = 1; Counter10 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter10++) {
          System.out.print("#");
          for (int Counter11 = 1; Counter11 <= (SizeOfBoxes-2); Counter11++) {
            System.out.print("-");
          }
          System.out.print("#");
          if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
          for (int Counter12 = 1; Counter12 <= LengthOfEdge; Counter12++) {
            System.out.print(" ");
            }
          } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
            for (int Counter12 = 1; Counter12 <= LengthOfEdge; Counter12++) {
              System.out.print("-");
            }
          }
        }
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
          int Temp5;
          System.out.print("#");
          if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
            for (Temp5 = 1; Temp5 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp5++) {
              System.out.print("-");
            }
          } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
            for (Temp5 = 1; Temp5 <= (SizeOfBoxes-2); Temp5++) {
              System.out.print("-");
            }
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp5+2)) > 0) {
              if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
                for (int Temp6 = 0; Temp6 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp5+2)); Temp6++) {
                  System.out.print(" ");
                }
              } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
                for (int Temp6 = 0; Temp6 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp5+2)); Temp6++) {
                  System.out.print("-");
                }
              }
            }
          }
        }
        System.out.println();
        //Forth line:
        for (int Counter13 = 1; Counter13 <= LengthOfEdge; Counter13++) {
          for (int Counter14 = 1; Counter14 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter14++) {
            for (int Counter15 = 1; Counter15 <= (SizeOfBoxes+LengthOfEdge); Counter15++) {
              if (Counter15 == (SizeOfBoxes/2) || Counter15 == ((SizeOfBoxes/2)+1)) { //Middle of the square 
                System.out.print("|");
              } else {
                System.out.print(" ");
              }
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0: 
              if ((Width%(SizeOfBoxes+LengthOfEdge)) >= (SizeOfBoxes/2)) {
                for (int Temp7 = 1; Temp7 < (SizeOfBoxes/2); Temp7++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if ((Width%(SizeOfBoxes+LengthOfEdge)) >= ((SizeOfBoxes/2)+1)) {
                  System.out.print("|");
                }
              }
            }
          System.out.println();
        }
      }
      if ((Height%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0 (height):
        //First line:
        for (int Counter16 = 1; Counter16 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter16++) {
          System.out.print("#");
          for (int Counter17 = 1; Counter17 <= (SizeOfBoxes-2); Counter17++) {
            System.out.print("-");
          }
          System.out.print("#");
          if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
          for (int Counter18 = 1; Counter18 <= LengthOfEdge; Counter18++) {
            System.out.print(" ");
            }
          } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
            for (int Counter18 = 1; Counter18 <= LengthOfEdge; Counter18++) {
              System.out.print("-");
            }
          }
        }
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
          int Temp8;
          System.out.print("#");
          if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
            for (Temp8 = 1; Temp8 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp8++) {
              System.out.print("-");
            }
          } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
            for (Temp8 = 1; Temp8 <= (SizeOfBoxes-2); Temp8++) {
              System.out.print("-");
            }
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)) > 0) {
              if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
                for (int Temp9 = 0; Temp9 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)); Temp9++) {
                  System.out.print(" ");
                }
              } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
                for (int Temp9 = 0; Temp9 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)); Temp9++) {
                  System.out.print("-");
                }
              }
            }
          }
        }
      }
      System.out.println();
      //Second line:
      if (((Height%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Height%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
        for (int Counter19 = 1; Counter19 <= ((Height%(SizeOfBoxes+LengthOfEdge))-1); Counter19++) {
          for (int Counter20 = 1; Counter20 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter20++) {
          System.out.print("|");
          for (int Counter21 = 1; Counter21 <= (SizeOfBoxes-2); Counter21++) {
            System.out.print(" ");
          }
          System.out.print("|");
          if (Counter19 == ((SizeOfBoxes-2)/2) || Counter19 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
            for (int Counter22 = 1; Counter22 <= LengthOfEdge; Counter22++) {
              System.out.print("-");
            }
          } else {
            for (int Counter23 = 1; Counter23 <= LengthOfEdge; Counter23++) {
              System.out.print(" ");
              }
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
              int Temp10;
              System.out.print("|");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                for (Temp10 = 1; Temp10 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp10++) {
                  System.out.print(" ");
                }
              } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                for (Temp10 = 1; Temp10 <= (SizeOfBoxes-2); Temp10++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)) > 0) {
                  if (Counter19 == ((SizeOfBoxes-2)/2) || Counter19 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
                    for (int Temp11 = 0; Temp11 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)); Temp11++) {
                      System.out.print("-");
                    }
                  } else {
                    for (int Temp11 = 0; Temp11 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)); Temp11++) {
                      System.out.print(" ");
                    }
                  }
                }
              }
            }
          System.out.println();
        }
      } else if (((Height%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
        for (int Counter19 = 1; Counter19 <= (SizeOfBoxes-2); Counter19++) {
          for (int Counter20 = 1; Counter20 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter20++) {
          System.out.print("|");
          for (int Counter21 = 1; Counter21 <= (SizeOfBoxes-2); Counter21++) {
            System.out.print(" ");
          }
          System.out.print("|");
          if (Counter19 == ((SizeOfBoxes-2)/2) || Counter19 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
            for (int Counter22 = 1; Counter22 <= LengthOfEdge; Counter22++) {
              System.out.print("-");
            }
          } else {
            for (int Counter23 = 1; Counter23 <= LengthOfEdge; Counter23++) {
              System.out.print(" ");
              }
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
              int Temp10;
              System.out.print("|");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                for (Temp10 = 1; Temp10 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp10++) {
                  System.out.print(" ");
                }
              } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                for (Temp10 = 1; Temp10 <= (SizeOfBoxes-2); Temp10++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)) > 0) {
                  if (Counter19 == ((SizeOfBoxes-2)/2) || Counter19 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
                    for (int Temp11 = 0; Temp11 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)); Temp11++) {
                      System.out.print("-");
                    }
                  } else {
                    for (int Temp11 = 0; Temp11 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp10+2)); Temp11++) {
                      System.out.print(" ");
                    }
                  }
                }
              }
            }
          System.out.println();
        }
      }
      //Third line:  
      if (((Height%(SizeOfBoxes+LengthOfEdge))-(SizeOfBoxes-1)) > 0) {
        for (int Counter16 = 1; Counter16 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter16++) {
          System.out.print("#");
          for (int Counter17 = 1; Counter17 <= (SizeOfBoxes-2); Counter17++) {
            System.out.print("-");
          }
          System.out.print("#");
          if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
            for (int Counter18 = 1; Counter18 <= LengthOfEdge; Counter18++) {
            System.out.print(" ");
            }
          } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
            for (int Counter18 = 1; Counter18 <= LengthOfEdge; Counter18++) {
              System.out.print("-");
            }
          }
        }
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
          int Temp8;
          System.out.print("#");
          if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
            for (Temp8 = 1; Temp8 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp8++) {
              System.out.print("-");
            }
          } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
            for (Temp8 = 1; Temp8 <= (SizeOfBoxes-2); Temp8++) {
              System.out.print("-");
            }
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)) > 0) {
              if (SizeOfBoxes > 2) { //If the size of the boxes is greater than 2:
                for (int Temp9 = 0; Temp9 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)); Temp9++) {
                  System.out.print(" ");
                }
              } else if (SizeOfBoxes == 2) { //If the size of the boxes is equal to 2:
                for (int Temp9 = 0; Temp9 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp8+2)); Temp9++) {
                  System.out.print("-");
                }
              }
            }
          }
        }
      }
      System.out.println();
      //Forth line:
      if (((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes) > 0) {
        if (((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes) <= LengthOfEdge) {
          for (int Counter13 = 1; Counter13 <= ((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes); Counter13++) {
            for (int Counter14 = 1; Counter14 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter14++) {
              for (int Counter15 = 1; Counter15 <= (SizeOfBoxes+LengthOfEdge); Counter15++) {
                if (Counter15 == (SizeOfBoxes/2) || Counter15 == ((SizeOfBoxes/2)+1)) { //Middle of the square 
                  System.out.print("|");
                } else {
                  System.out.print(" ");
                }
              }
            }
            if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
              if ((Width%(SizeOfBoxes+LengthOfEdge)) >= (SizeOfBoxes/2)) { 
                for (int Temp7 = 1; Temp7 < (SizeOfBoxes/2); Temp7++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if ((Width%(SizeOfBoxes+LengthOfEdge)) >= ((SizeOfBoxes/2)+1)) { 
                  System.out.print("|");
                }
              }
            }
            System.out.println();
            }
          }
        }
      } else if ((SizeOfBoxes > Width) && (SizeOfBoxes > Height)) { //If the size of the boxes is greater than the display window: 
        System.out.print("#"); 
        for (int Counter = 1; Counter <= (Width-1); Counter++) { 
          System.out.print("-"); 
        }
        System.out.println();
        for (int Counter = 1; Counter <= (Height-1); Counter++) {
          System.out.print("|");
          System.out.println();
        }
      }
    } else { //Odd squares: 
      if ((SizeOfBoxes <= Width) && (SizeOfBoxes <= Height)) { //If the size of the boxes is less than or equal to the display window:
      //First line:  
      for (int Counter1 = 1; Counter1 <= (Height/(SizeOfBoxes+LengthOfEdge)); Counter1++) {
        for (int Counter2 = 1; Counter2 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter2++) {
          System.out.print("#");
          for (int Counter3 = 1; Counter3 <= (SizeOfBoxes-2); Counter3++) {
            System.out.print("-");
          }
          if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
            System.out.print("#");
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) {
              System.out.print(" ");
            }
          } else if (SizeOfBoxes == 1) { //If the size of the boxes is equal to 1:
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) {
              System.out.print("-");
            }
          }
        }
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0: 
          if (SizeOfBoxes == 1) { //If the size of the boxes is equal to 1:
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= LengthOfEdge) {
              for (int Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            }
          } else { //If the size of the boxes is greater than 1:
            int Temp;
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
              for (Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
              for (Temp = 1; Temp <= (SizeOfBoxes-2); Temp++) {
                System.out.print("-");
              }
              System.out.print("#");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)) > 0) {
                for (int Temp2 = 1; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                  System.out.print(" ");
                }
              } 
            }
          }
        }
        if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
          System.out.println();
          //Second line:
          for (int Counter5 = 1; Counter5 <= (SizeOfBoxes-2); Counter5++) {
            for (int Counter6 = 1; Counter6 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter6++) {
              System.out.print("|");
              for (int Counter7 = 1; Counter7 <= (SizeOfBoxes-2); Counter7++) {
                System.out.print(" ");
              }
              System.out.print("|");
              if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
                for (int Counter8 = 1; Counter8 <= LengthOfEdge; Counter8++) {
                  System.out.print("-");
                }
              } else {
                for (int Counter9 = 1; Counter9 <= LengthOfEdge; Counter9++) {
                  System.out.print(" ");
                }
              }
            }
            if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
              int Temp3;
              System.out.print("|");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                for (Temp3 = 1; Temp3 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp3++) {
                  System.out.print(" ");
                }
              } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                for (Temp3 = 1; Temp3 <= (SizeOfBoxes-2); Temp3++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)) > 0) {
                  if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
                    for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                      System.out.print("-");
                    }
                  } else {
                    for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                      System.out.print(" ");
                    }
                  }
                }
              }
            }
            System.out.println();
          }
        }
        if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
          //Third line:
          for (int Counter10 = 1; Counter10 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter10++) {
            System.out.print("#");
            for (int Counter11 = 1; Counter11 <= (SizeOfBoxes-2); Counter11++) {
              System.out.print("-");
            }
            System.out.print("#");
            for (int Counter12 = 1; Counter12 <= LengthOfEdge; Counter12++) {
              System.out.print(" ");
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
          if (SizeOfBoxes == 1) { //If the size of the boxes is equal to 1:
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= LengthOfEdge) {
              for (int Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            }
          } else { //If the size of the boxes is greater than 1:
            int Temp;
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
              for (Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
              for (Temp = 1; Temp <= (SizeOfBoxes-2); Temp++) {
                System.out.print("-");
              }
              System.out.print("#");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)) > 0) {
                for (int Temp2 = 1; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                  System.out.print(" ");
                  }
                } 
              }
            }
          }
        }
        System.out.println();
        //Forth line:
        for (int Counter13 = 1; Counter13 <= LengthOfEdge; Counter13++) {
          for (int Counter14 = 1; Counter14 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter14++) {
            for (int Counter15 = 1; Counter15 <= (SizeOfBoxes+LengthOfEdge); Counter15++) {
              if (Counter15 == ((SizeOfBoxes/2)+1)) { //Middle of the square 
                System.out.print("|");
              } else {
                System.out.print(" ");
              }
            }
          }
          if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder is different from 0: 
            if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
              if ((Width%(SizeOfBoxes+LengthOfEdge)) >= ((SizeOfBoxes/2)+1)) {
                for (int Temp7 = 1; Temp7 <= (SizeOfBoxes/2); Temp7++) {
                  System.out.print(" ");
                }
                System.out.print("|");
              }
            } else { //If the size of the boxes is equal to 1:
              if ((Width%(SizeOfBoxes+LengthOfEdge)) <= LengthOfEdge) {
                System.out.print("|");
              }
            }  
          }
          System.out.println();
        } 
      }
      if ((Height%(SizeOfBoxes+LengthOfEdge)) != 0) { //if the remainder of the division is different from 0 (height): 
        //First line:
        for (int Counter2 = 1; Counter2 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter2++) {
          System.out.print("#");
          for (int Counter3 = 1; Counter3 <= (SizeOfBoxes-2); Counter3++) {
            System.out.print("-");
          }
          if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
            System.out.print("#");
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) {
              System.out.print(" ");
            }
          } else if (SizeOfBoxes == 1) { //If the size of the boxes is equal to 1:
            for (int Counter4 = 1; Counter4 <= LengthOfEdge; Counter4++) {
              System.out.print("-");
            }
          }
        }
        if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder od the division is different from 0:
          if (SizeOfBoxes == 1) { //If the size of the boxes is equal to 1:
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= LengthOfEdge) {
              for (int Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            }
          } else { //If the size of the boxes is greater than 1:
            int Temp;
            System.out.print("#");
            if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
              for (Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                System.out.print("-");
              }
            } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
              for (Temp = 1; Temp <= (SizeOfBoxes-2); Temp++) {
                System.out.print("-");
              }
              System.out.print("#");
              if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)) > 0) {
                for (int Temp2 = 1; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                  System.out.print(" ");
                }
              } 
            }
          }
        }
        if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
          System.out.println();
          if (((Height%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Height%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
            //Second line:
            for (int Counter5 = 1; Counter5 <= ((Height%(SizeOfBoxes+LengthOfEdge))-1); Counter5++) {
              for (int Counter6 = 1; Counter6 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter6++) {
                System.out.print("|");
                for (int Counter7 = 1; Counter7 <= (SizeOfBoxes-2); Counter7++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
                  for (int Counter8 = 1; Counter8 <= LengthOfEdge; Counter8++) {
                    System.out.print("-");
                  }
                } else {
                  for (int Counter9 = 1; Counter9 <= LengthOfEdge; Counter9++) {
                    System.out.print(" ");
                  }
                }
              }
              if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
                int Temp3;
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                  for (Temp3 = 1; Temp3 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp3++) {
                    System.out.print(" ");
                  }
                } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                  for (Temp3 = 1; Temp3 <= (SizeOfBoxes-2); Temp3++) {
                    System.out.print(" ");
                  }
                  System.out.print("|");
                  if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)) > 0) {
                    if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
                      for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                        System.out.print("-");
                      }
                    } else {
                      for (int Temp4 = 0; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                        System.out.print(" ");
                      }
                    }
                  }
                } 
              }
              System.out.println();
            }
          } else if (((Height%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
            for (int Counter5 = 1; Counter5 <= (SizeOfBoxes-2); Counter5++) {
              for (int Counter6 = 1; Counter6 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter6++) {
                System.out.print("|");
                for (int Counter7 = 1; Counter7 <= (SizeOfBoxes-2); Counter7++) {
                  System.out.print(" ");
                }
                System.out.print("|");
                if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square 
                  for (int Counter8 = 1; Counter8 <= LengthOfEdge; Counter8++) {
                    System.out.print("-");
                  }
                } else {
                  for (int Counter9 = 1; Counter9 <= LengthOfEdge; Counter9++) {
                    System.out.print(" ");
                  }
                }
              }
              if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
                int Temp3;
                System.out.print("|");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                  for (Temp3 = 1; Temp3 <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp3++) {
                    System.out.print(" ");
                  }
                } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                  for (Temp3 = 1; Temp3 <= (SizeOfBoxes-2); Temp3++) {
                    System.out.print(" ");
                  }
                  System.out.print("|");
                  if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)) > 0) {
                    if (Counter5 == (((SizeOfBoxes-2)/2)+1)) { //Middle of the square
                      for (int Temp4 = 1; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                        System.out.print("-");
                      }
                    } else {
                      for (int Temp4 = 1; Temp4 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp3+2)); Temp4++) {
                        System.out.print(" ");
                      }
                    }
                  }
                }
              }
              System.out.println();
            }
          }
        }
        if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
          if (((Height%(SizeOfBoxes+LengthOfEdge))-(SizeOfBoxes-1)) > 0) {
            //Third line:
            for (int Counter10 = 1; Counter10 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter10++) {
              System.out.print("#");
              for (int Counter11 = 1; Counter11 <= (SizeOfBoxes-2); Counter11++) {
                System.out.print("-");
              }
              System.out.print("#");
              for (int Counter12 = 1; Counter12 <= LengthOfEdge; Counter12++) {
                System.out.print(" ");
              }
            }
            if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0: 
              if (SizeOfBoxes == 1) {
                System.out.print("#");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= LengthOfEdge) {
                  for (int Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                    System.out.print("-");
                  }
                }
              } else {
                int Temp;
                System.out.print("#");
                if (((Width%(SizeOfBoxes+LengthOfEdge))-1) <= (SizeOfBoxes-2) && ((Width%(SizeOfBoxes+LengthOfEdge))-1) > 0) {
                  for (Temp = 1; Temp <= ((Width%(SizeOfBoxes+LengthOfEdge))-1); Temp++) {
                    System.out.print("-");
                  }
                } else if (((Width%(SizeOfBoxes+LengthOfEdge))-1) > (SizeOfBoxes-2)) {
                  for (Temp = 1; Temp <= (SizeOfBoxes-2); Temp++) {
                    System.out.print("-");
                  }
                  System.out.print("#");
                  if (((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)) > 0) {
                    for (int Temp2 = 1; Temp2 <= ((Width%(SizeOfBoxes+LengthOfEdge))-(Temp+2)); Temp2++) {
                      System.out.print(" ");
                    }
                  } 
                }
              }
            }
          }
        }
        System.out.println();
        if (((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes) > 0) {
          if (((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes) <= LengthOfEdge) {
            //Forth line:
            for (int Counter13 = 1; Counter13 <= ((Height%(SizeOfBoxes+LengthOfEdge))-SizeOfBoxes); Counter13++) {
              for (int Counter14 = 1; Counter14 <= (Width/(SizeOfBoxes+LengthOfEdge)); Counter14++) {
                for (int Counter15 = 1; Counter15 <= (SizeOfBoxes+LengthOfEdge); Counter15++) {
                  if (Counter15 == ((SizeOfBoxes/2)+1)) { //Middle of the square 
                    System.out.print("|");
                  } else {
                    System.out.print(" ");
                  }
                }
              }
              if ((Width%(SizeOfBoxes+LengthOfEdge)) != 0) { //If the remainder of the division is different from 0:
                if (SizeOfBoxes > 1) { //If the size of the boxes is greater than 1:
                  if ((Width%(SizeOfBoxes+LengthOfEdge)) >= ((SizeOfBoxes/2)+1)) {
                    for (int Temp7 = 1; Temp7 <= (SizeOfBoxes/2); Temp7++) {
                      System.out.print(" ");
                    }
                    System.out.print("|");
                  }
                } else { //If the size of the boxes is equal to 1:
                  if ((Width%(SizeOfBoxes+LengthOfEdge)) <= LengthOfEdge) {
                    System.out.print("|");
                  }
                }  
              }
              System.out.println();
              }
            }
          }
        }
      } else if ((SizeOfBoxes > Width) && (SizeOfBoxes > Height)) { //If the size of the boxes is greater than the display window: 
        System.out.print("#"); 
        for (int Counter = 1; Counter <= (Width-1); Counter++) { 
          System.out.print("-"); 
        }
        System.out.println();
        for (int Counter = 1; Counter <= (Height-1); Counter++) {
          System.out.print("|");
          System.out.println();
        }
      }
    }
    
  } //End of main method
  
} //End of class