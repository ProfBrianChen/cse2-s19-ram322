//////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass {
  
  public static void main(String args[]) {
    /// Prints a welcome message from me using my Lehigh Network ID as a signature to the Terminal Box
    System.out.println("  ----------- \n  | Welcome | \n  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ \n / \\/ \\/ \\/ \\/ \\/ \\ \n<-R--A--M--3--2--2->\n \\ /\\ /\\ /\\ /\\ /\\ / \n  v  v  v  v  v  v");
    System.out.println("  My name is Rafaela Mantoan Borges and I was born and raised in Brazil. \n  However, I also have dual citizenship which makes me an Italian citizen as well.");
    
  }
  
}