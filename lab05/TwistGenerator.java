//////////////
//// CSE2 Twist Generator 
//// 03.01.19 Rafaela Mantoan Borges
//// This program should:
// Use the Scanner class to obtain from the user a positive integer 
// Check to make sure that the user provided a positive integer and if not, use an infinite loop to ask again
// Use the acceptable integer to indicate the user’s desired twist length and print it out 


import java.util.Scanner; //Imports Scanner class (import statement)

public class TwistGenerator {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner( System.in ); //Declares and constructs an instance for myScanner1
    
    //Input data 
    System.out.print("Enter a positive integer to be the twist length: "); //Prompts the user for a positive integer to be the twist length 
    boolean Condition = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int Length = 0; //Declares and assigns the twist length to an integer
    
    if (Condition) { //If input is available to be read (if the condition is true):
      Length = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement 
    do { //Does while the condition is false (input is not available to be read) or the twist length is less than or equal to 0:
      if (Condition) { //If input is available to be read (if the condition is true):
        if (Length <= 0) { //If the twist length is less than or equal to 0:
        System.out.print("Enter a positive integer to be the twist length: "); //Prompts the user for a positive integer to be the twist length 
        Condition = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition) { //If input is available to be read (if the condition is true):
          Length = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        String junkWord = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a positive integer to be the twist length: "); //Prompts the user for a positive integer to be the twist length 
        Condition = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition) { //If input is available to be read (if the condition is true):
          Length = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } while ((Condition == false) || (Length <= 0)); //End of do-while loop
    
    //Prints out the twist    
    //First line 
    for (int counter = 1; counter <= (Length/3); counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to the twist length divided by 3
      System.out.print("\\ /"); //Prints out the statement to the terminal window
    } //End of for loop
    
    int Remainder = Length%3; //Calculates the remainder of the division between the twist length and 3 and declares it as an integer variable 
    switch (Remainder) { //Evaluates the value of the remainder
      case 1: //If the remainder is 1:
        System.out.print("\\"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
      case 2: //If the remainder is 2:
        System.out.print("\\ "); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
    } //End of switch statement
    
    System.out.print("\n"); //Inserts a new line to the text
    
    //Second line
    for (int counter = 1; counter <= (Length/3); counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to the twist length divided by 3
      System.out.print(" X "); //Prints out the statement to the terminal window
    } //End of for loop
    
    switch (Remainder) { //Evaluates the value of the remainder
      case 1: //If the remainder is 1:
        System.out.print(" "); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
      case 2: //If the remainder is 2:
        System.out.print(" X"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
    } //End of switch statement
   
    System.out.print("\n"); //Inserts a new line to the text
    
    //Third line
    for (int counter = 1; counter <= (Length/3); counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to the twist length divided by 3
      System.out.print("/ \\"); //Prints out the statement to the terminal window
    } //End of for loop
    
    switch (Remainder) { //Evaluates the value of the remainder
      case 1: //If the remainder is 1:
        System.out.print("/"); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
      case 2: //If the remainder is 2:
        System.out.print("/ "); //Prints out the statement to the terminal window
        break; //Breaks out of the condition
    } //End of switch statement
    
    System.out.print("\n"); //Inserts a new line to the text
    
  } //End of main method
  
} //End of class
         