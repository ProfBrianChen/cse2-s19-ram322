//////////////
//// CSE2 Play Lottery 
//// 04/09/19 Rafaela Mantoan Borges 
//// This program should:
// Take a series of 5 integers as input from the user and store them in an array
// Generate a random series of 5 integers in the range of 0 to 59, without replacement, and store them in another array
// Compare the arrays 

import java.util.Arrays; 
import java.util.Scanner;
import java.util.Random;
import java.lang.Character;

public class PlayLottery {
  
  //Method to generate random numbers for the lottery without duplication
  public static int[] numbersPicked () { //This method returns an array of integers
    Random randomGenerator = new Random(); //Declares and constructs an instance
    int[] LotteryNumbers = new int[5]; //Declares and allocates space for an array of 5 elements  
    System.out.print("The winning numbers are: "); //Prints out the statement to the terminal window
    for (int i = 0; i < 5; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to 5
      LotteryNumbers[i] = randomGenerator.nextInt(59 + 1); //Generates an integer between 0 and 59 (inclusive)
      switch (i) { //Evaluate i:
        case 1: //If i is 1: 
          do { //Does this while the ith element is a repetitive number:
            if (LotteryNumbers[i] == LotteryNumbers[i-1]) { //If the ith element is a repetitive number:
              LotteryNumbers[i] = randomGenerator.nextInt(59 + 1); //Generates another integer between 0 and 59 (inclusive)
            } //End of if statement 
          } while (LotteryNumbers[i] == LotteryNumbers[i-1]); //End of do-while loop 
          break; //Breaks out of the condition 
        case 2: //If i is 2: 
          do { //Does this while the ith element is a repetitive number:
            if (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2]) { //If the ith element is a repetitive number
              LotteryNumbers[i] = randomGenerator.nextInt(59 + 1); //Generates another integer between 0 and 59 (inclusive)
            } //End of if statement 
          } while (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2]); //End of do-while loop 
          break; //Breaks out of the condition 
        case 3: //If i is 3: 
          do { //Does this while the ith element is a repetitive number:
            if (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2] || LotteryNumbers[i] == LotteryNumbers[i-3]) { //If the ith element is a repetitive number
              LotteryNumbers[i] = randomGenerator.nextInt(59 + 1); //Generates another integer between 0 and 59 (inclusive)
            } //End of if statement 
          } while (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2] || LotteryNumbers[i] == LotteryNumbers[i-3]); //End of do-while loop 
          break; //Breaks out of the condition
        case 4: //If i is 4: 
          do { //Does this while the ith element is a repetitive number:
            if (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2] || LotteryNumbers[i] == LotteryNumbers[i-3] || LotteryNumbers[i] == LotteryNumbers[i-4]) { //If the ith element is a repetitive number
              LotteryNumbers[i] = randomGenerator.nextInt(59 + 1); //Generates another integer between 0 and 59 (inclusive)
            } //End of if statement 
          } while (LotteryNumbers[i] == LotteryNumbers[i-1] || LotteryNumbers[i] == LotteryNumbers[i-2] || LotteryNumbers[i] == LotteryNumbers[i-3] || LotteryNumbers[i] == LotteryNumbers[i-4]); //End of do-while loop 
          break; //Breaks out of the condition 
      } //End of switch statement
      System.out.print(LotteryNumbers[i] + " "); //Prints out the elements of the array to the terminal window
    } //End of for loop 
    System.out.println(); //Jumps to the next line 
    return LotteryNumbers; //Returns the array 
  } //End of numbersPicked method 
  
  //Method to compare arrays 
  public static boolean userWins (int[] YourNumbers, int[] LotteryNumbers) { //This method accepts two integer arrays and returns a boolean
    if (Arrays.equals(YourNumbers, LotteryNumbers)) { //If arrays are equal: 
      return true; //Returns true 
    } else { //If arrays don't match:
      return false; //Returns false;
    } //End of if statement
  } //End of userWins method 
  
  //Main method: 
  public static void main (String[] args) { //Main method required for every Java program 
    Scanner myScanner = new Scanner(System.in); //Declares and constructs an instance for myScanner
    int[] YourNumbers = new int[5]; //Declares and allocates space for an array of 5 elements 
    for (int i = 0; i < 5; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to 5
      System.out.print("Enter number " + (i+1) + " between 0 and 59: "); //Prompts the user for an integer between 0 and 59
      boolean Condition = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
      int YourNumber = 0; //Declares and assigns your number to an integer
      if (Condition) { //If input is available to be read (if the condition is true):
        YourNumber = myScanner.nextInt(); //Accepts and returns the integer value  
      } //End of if statement 
      do { //Does while the condition is false (input is not available to be read) or your number is not between 0 and 59:
        if (Condition) { //If input is available to be read (if the condition is true):
          if (!(0 <= YourNumber) || !(YourNumber <= 59)) { //If your number is not between 0 and 59:
            System.out.println("ERROR: INTEGER BETWEEN 0 AND 59 NEDEED"); //Prints out the error message to the terminal window 
            System.out.print("Enter number " + (i+1) + " between 0 and 59: "); //Prompts the user for an integer between 0 and 59
            Condition = myScanner.hasNextInt(); //Checks if the input is available to be read
            if (Condition) { //If input is available to be read (if the condition is true):
              YourNumber = myScanner.nextInt(); //Accepts and returns the integer value  
            } //End of if statement
          } //End of if statement
        } else { //If the condition is false (input is not available to be read):
          System.out.println("ERROR: INPUT OF INTEGER TYPE NEDEED"); //Prints out the error message to the terminal window
          String junkWord = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
          System.out.print("Enter number " + (i+1) + " between 0 and 59: "); //Prompts the user for an integer between 0 and 59
          Condition = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition) { //If input is available to be read (if the condition is true):
            YourNumber = myScanner.nextInt(); //Accepts and returns the integer value  
          } //End of if statement
        } //End of if statement
      } while ((Condition == false) || !(0 <= YourNumber) || !(YourNumber <= 59)); //End of do-while loop
      YourNumbers[i] = YourNumber; //Assigns the ith element of the array to your number 
    } //End of for loop 
    System.out.print("Your numbers are: "); //Prints out the statement to the terminal window
    for (int j = 0; j < 5; j++) { //Declares and initializes a counter incrementing it by 1 until it is equal to 5
      System.out.print(YourNumbers[j] + " "); //Prints out the elements of the array to the terminal window
    } //End of for loop 
    System.out.println(); //Jumps to the next line 
    int[] LotteryNumbers = numbersPicked(); //Calls numbersPicked method and stores the new array 
    boolean Result = userWins (YourNumbers, LotteryNumbers); //Calls userWins method to compare the arrays and stores the output into a boolean
    if (Result) { //If the boolean is true:
      System.out.println("You win"); //Prints out the winning message to the terminal window
    } else { //If the boolean is false:
      System.out.println("You lose"); //Prints out the message to the terminal window
    } //End of if statement
  } //End of main method 
  
} //End of class