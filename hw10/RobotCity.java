//////////////
//// CSE2 Robot City 
//// 05/02/19 Rafaela Mantoan Borges

import java.util.Arrays; //import Arrays class
import java.util.Random; //import Random class

public class RobotCity {
  
  //Method for generating the city:
  public static int[][] buildCity () { //This method returns a multidimensional array 
    Random randomGenerator = new Random(); //Declares and constructs an instance
    int eastWest = randomGenerator.nextInt((15-10) + 1) + 10; //Generates an integer between 10 and 15 (inclusive) to be the east-west dimension of the city (columns)
    int northSouth = randomGenerator.nextInt((15-10) + 1) + 10; //Generates an integer between 10 and 15 (inclusive) to be the north-south dimension of the city (rows)
    int[][] cityArray = new int[eastWest][northSouth]; //Declares and allocates space for an array of length equal to the east-west dimension of the city (columns)
    for (int i = 0; i < eastWest; i++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the east-west dimension of the city (columns)
      for (int j = 0; j < northSouth; j++) { //Declares and initializes a counter incrementing it by 1 until it is equal to the north-south dimension of the city (rows)
        cityArray[i][j] = randomGenerator.nextInt((999-100) + 1) + 100; //Generates an integer between 999 and 100 (inclusive) to be population of the block
      } //End of for loop 
    } //End of for loop 
    return cityArray;
  } //End of buildCity method 
  
  //Method for displaying the array: 
  public static void display (int[][] cityArray) { //This method accepts a multidimensional array 
    for (int row = 0; row < cityArray[0].length; row++) { //Initializes a counter incrementing it by one until it is equal to the length of the members of the array 
      for (int column = 0; column < cityArray.length; column++) { //Initializes a counter incrementing it by one until it is equal to the length of the array
        System.out.printf("%4d ", cityArray[column][row]); //Prints the array using the printf() format string
      } //End of for loop 
      System.out.printf("\n"); //Jumps to another line 
    } //End of for loop 
  } //End of display method
  
  //Method for invading the city:
  public static void invade (int[][] cityArray, int k) { //This method accepts a multidimensional array and an integer
    for (int i = 0; i < k; i++) { //Initializes a counter until it is equal to the number of invading robots
      Random randomGenerator = new Random(); //Declares and constructs an instance
      int latitude = randomGenerator.nextInt((cityArray.length-1) + 1); //Generates a random integer to be the latitude  
      int longitude = randomGenerator.nextInt((cityArray[0].length-1) + 1); //Generates a random integer to be the longitude
      if (cityArray[latitude][longitude] < 0) { //If the block is already invated (number is negative):
        do { //does while the integer value of the landing block is the negative:
          latitude = randomGenerator.nextInt((cityArray.length-1) + 1); //Generates another random integer to be the latitude 
          longitude = randomGenerator.nextInt((cityArray[0].length-1) + 1); //Generates another random integer to be the longitude
        } while (cityArray[latitude][longitude] < 0); //End of do-while loop
      } //End of if statement
      cityArray[latitude][longitude] = - cityArray[latitude][longitude]; //Sets the integer value of the landing block to the negative value of its original value to denote the occupancy of the robot
    } //End of for loop
  } //End of invade method  
  
  //Method for updating the city:
  public static void update (int[][] cityArray) { //This method accepts a multidimensional array
    for (int row = cityArray[0].length-1; row >= 0; row--) { //Initializes a counter decrementing it by one until it is less them the length of the members of the array 
      for (int column = cityArray.length-1; column >= 0; column--) { //Initializes a counter decrementing it by one until it is less them the length of the array
        if (cityArray[column][row] < 0) { //If the block is invated:
          if (column == cityArray.length-1) { //If the robots are at the eastern-most limits of the city:
            cityArray[column][row] = - cityArray[column][row]; //Sets the integer value of the block back to positive 
          } else { //If not:
            cityArray[column][row] = - cityArray[column][row]; //Sets the integer value of the block back to positive
            cityArray[column+1][row] = - cityArray[column+1][row]; //Sets the integer value of the next block eastwards to negative
          } //End of if statement
        } //End of if statement
      } //End of for loop 
    } //End of for loop 
  } //End of update method
  
  //Main method:
  public static void main (String[] args) { //Main method required for every Java program
    Random randomGenerator = new Random(); //Declares and constructs an instance
    int[][] cityArray = buildCity(); //Calls buildCity method 
    System.out.println(" City:"); //Prints out the statement to the terminal window
    display(cityArray); //Prints out the array by calling display method      
    int k = randomGenerator.nextInt(((cityArray.length*cityArray[0].length)-1) + 1) + 1; //Generates a random integer to be the number of invading robots
    invade(cityArray, k); //Calls invade method 
    System.out.println(); //Jumps to another line
    System.out.println(" Invated City:"); //Prints out the statement to the terminal window
    display(cityArray); //Prints out the array by calling display method 
    for (int i = 1; i < 6; i++) { //Initializes a counter by incrementing it by one until it is equal to six to see the movement of the invading robots
      update(cityArray); //Calls update method 
      System.out.println(); //Jumps to another line
      System.out.println(" Updated City " + i + ":"); //Prints out the statement to the terminal window
      display(cityArray); //Prints out the array by calling display method
      if (i != 5) { //If i is different than 5:
        System.out.println(); //Jumps to another line
      } //End of if statement
    } //End of for loop
  } //End of main method
  
} //End of class