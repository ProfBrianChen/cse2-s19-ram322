//////////////
//// CSE2 Area 
//// 03.26.19 Rafaela Mantoan Borges 
//// This program should:
// Prompt the user to choose which shape they want by typing the words “rectangle”, “triangle” or “circle” (without caps)
// Prompt the user to enter the values of the dimension(s) appropriate for the chosen shape in the form of doubles
// Then calculate the area of the chosen shape 

import java.util.Scanner; //Imports Scanner class (import statement)
import java.lang.Math; //Imports Math class (import statement)

public class Area { 
  
  //Method to calculate the area of a rectangle
  public static double RectangleArea (double Length, double Height) { //The method accepts two double inputs and returns a double variable 
    double AreaOfRectangle = Length*Height; //Calculates the area of the rectangle by multiplying the two inputs and stores the value in a double variable 
    return AreaOfRectangle; //Returns the area of the rectangle
  } //End of RectangleArea method
  
  //Method to calculate the area of a triangle
  public static double TriangleArea (double Length, double Height) { //The method accepts two double inputs and returns a double variable 
    double AreaOfTriangle = (Length*Height)/2; //Calculates the area of the triangle by multiplying the two inputs and dividing it by 2 and stores the value in a double variable
    return AreaOfTriangle; //Returns the area of the triangle
  } //End of TriangleArea method
  
  //Method to calculate the area of a circle
  public static double CircleArea (double Radius) { //The method accepts a double input and returns a double variable 
    double AreaOfCircle = Math.PI*Math.pow(Radius, 2); //Calculates the area of the circle by using methods from Math class and stores the value in a double variable
    return AreaOfCircle; //Returns the area of the circle
  } //End of CircleArea method
  
  //Method to use the Scanner class to obtain from the user a positive double 
  public static double Input (String Prompt) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner(System.in); //Declares and constructs an instance
    
    //Input data 
    System.out.print(Prompt); //Prompts the user for a positive double 
    boolean Condition = myScanner.hasNextDouble(); //Declares and assigns a boolean to check if the input is available to be read
    double Variable = 0.0; //Declares and assigns variable to a double 

    if (Condition) { //If input is available to be read (if the condition is true):
      Variable = myScanner.nextDouble(); //Accepts and returns the double value  
    } //End of if statement 
    do { //Does while the condition is false (input is not available to be read) or the variable is less than or equal to 0:
      if (Condition) { //If input is available to be read (if the condition is true):
        if (Variable <= 0) { //If the variable is less than or equal to 0:
          System.out.println("ERROR: POSITIVE DOUBLE NEEDED"); //Prints out the error message to the terminal window 
          System.out.print(Prompt); //Prompts the user for a positive double 
          Condition = myScanner.hasNextDouble(); //Checks if the input is available to be read
          if (Condition) { //If input is available to be read (if the condition is true):
            Variable = myScanner.nextDouble(); //Accepts and returns the double value  
          } //End of if statement
        } //End of if statement
      } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF DOUBLE TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print(Prompt); //Prompts the user for a positive double
        Condition = myScanner.hasNextDouble(); //Checks if the input is available to be read
        if (Condition) { //If input is available to be read (if the condition is true):
          Variable = myScanner.nextDouble(); //Accepts and returns the double value  
        } //End of if statement
      } //End of if statement
    } while ((Condition == false) || (Variable <= 0)); //End of do-while loop
    
    return Variable; //Returns the double variable
  } //End of Input method
  
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner(System.in); //Declares and constructs an instance
    
    //Shape type
    System.out.print("Enter the shape type: "); //Prompts the user for the shape type
    boolean Condition = myScanner.hasNext(); //Declares and assigns a boolean to check if the input is available to be read
    String Shape = ""; //Declares and assigns the shape type to a string  
       
    if (Condition) { //If input is available to be read (if the condition is true):
      Shape = myScanner.next(); //Accepts and returns the string value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the shape type is different from the acceptable ones
      if (Condition) { //If input is available to be read (if the condition is true):
        if (!(Shape.equals("rectangle") || Shape.equals("triangle") || Shape.equals("circle"))) { //If the shape type is different from the acceptable ones
          System.out.println("ERROR: ACCEPTABLE SHAPE NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter a shape type: "); //Prompts the user for the shape type
          Condition = myScanner.hasNext(); //Checks if the input is available to be read
          if (Condition) { //If input is available to be read (if the condition is true)
            Shape = myScanner.next(); //Accepts and returns the string value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR:INPUT OF STRING TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter shape type: "); //Prompts the user for the shape type
        Condition = myScanner.hasNext(); //Checks if the input is available to be read
        if (Condition) { //If input is available to be read (if the condition is true):
          Shape = myScanner.next(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition == false) || !(Shape.equals("rectangle") || Shape.equals("triangle") || Shape.equals("circle"))); //End of do-while loop
    
    if (Shape.equals("rectangle")) { //If shape equals rectagle:
      double Length = Input("Enter the length of the rectangle: "); //Calls Input method and stores the value of the length in a double variable 
      double Height = Input("Enter the height of the rectangle: "); //Calls Input method and stores the value of the height in a double variable
      double AreaOfRectangle = RectangleArea(Length, Height); //Calls RectangleArea method and stores the value of the area in a double variable
      System.out.println("The area of the rectangle is " + AreaOfRectangle + "."); //Prints out the output to terminal window
    } else if (Shape.equals("triangle")) { //If shape equals triangle:
      double Length = Input("Enter the length of the base of the triangle: "); //Calls Input method and stores the value of the length in a double variable 
      double Height = Input("Enter the height of the triangle: "); //Calls Input method and stores the value of the height in a double variable
      double AreaOfTriangle = TriangleArea(Length, Height); //Calls TriangleArea method and stores the value of the area in a double variable
      System.out.println("The area of the triangle is " + AreaOfTriangle + "."); //Prints out the output to terminal window
    } else if (Shape.equals("circle")) { //If shape equals circle:
      double Radius = Input("Enter the radius of the circle: "); //Calls Input method and stores the value of the radius in a double variable
      double AreaOfCircle = CircleArea(Radius); //Calls CircleArea method and stores the value of the area in a double variable
      System.out.println("The area of the circle is " + AreaOfCircle + "."); //Prints out the output to terminal window
    } //End of if statement
    
  } //End of main method
  
} //End of class