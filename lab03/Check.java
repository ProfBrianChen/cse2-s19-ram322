//////////////
//// CSE 02 Check 
//// 02.08.19 Rafaela Mantoan Borges
//// Given the fact that the user has gone out to dinner with friends and they decide to split the check evenly, this program should:
// Use the Scanner class to obtain from the user the original cost of the check
// Use the Scanner class to obtain from the user the percentage tip they wish to pay
// Use the Scanner class to obtain from the user the number of ways the check will be split
// Calculate and print out how much each person in the group needs to spend in order to pay the check

import java.util.Scanner; //Imports Scanner class (import statement)

public class Check {
  //Main method required for every Java program
  public static void main(String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner( System.in ); //Declares and constructs an instance
    
    //Input data
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Prompts the user for the original cost of the check 
    double CheckCost = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called CheckCost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Prompts the user for the tip percentage they wish to pay
    double TipPercent = myScanner.nextDouble(); //Reads and accepts the user input from STDIN and stores it in a double variable called TipPercent
    TipPercent /= 100; //Converts the tip percentage into a decimal number 
    System.out.print("Enter the number of people who went out to dinner: "); //Prompsts the user for the number of people who went out to dinner
    int NumberPeople = myScanner.nextInt(); //Reads and accepts the user input from STDIN and stores it in an integer variable called NumberPeople
    
    //Output data
    double TotalCost, CostPerPerson; //Declares the total cost of the check and the cost per person
    int Dollars, //Declares the whole dollar amount of cost as an integer variable 
    Dimes, Pennies; //Declares variables for storing digits to right to the decimal point for the cost
    TotalCost = CheckCost*(1+TipPercent); //Calculates the total cost of the check (including the tip percentage) 
    CostPerPerson = TotalCost/NumberPeople; //Divides the total cost of the check by the number of people and stores the result in the variable CostPerPerson
    Dollars = (int) CostPerPerson; //Casts the double variable, CostPerPerson, to an integer variable (explict casting) to get only the whole amount, droping the decimal fraction
    Dimes = (int) (CostPerPerson*10)%10; //Casts the double variable, CostPerPerson, to an integer variable (explict casting) to get the dimes amount by using the modulus operator to return the remainder of two numbers 
    Pennies = (int) (CostPerPerson*100)%10; //Casts the double variable, CostPerPerson, to an integer variable (explict casting) to get the pennies amount by using the modulus operator to return the remainder of two numbers 
    
    //Prints out output data
    System.out.println("Each person in the group owes $" + Dollars + "." + Dimes + Pennies); //Prints out the cost per person 
    
  } //End of main method
  
} //End of class