//////////////
//// CSE 02 Cyclometer 
//// 02.01.19 Rafaela Mantoan Borges
//// For two bicycle trips, given time and rotation count, this program should:
// Calculate and print out the number of minutes for each trip 
// Calculate and print out the number of counts for each trip
// Calculate and print out the distance of each trip in miles
// Calculate and print out the distance for the two trips combined

public class Cyclometer { 
  //Main Method 
  public static void main(String[] args) {
    
    //Input data
    int SecsTrip1 = 480; //Declares and assigns the time elapsed in seconds during trip 1 to an integer variable
    int SecsTrip2 = 3220; //Declares and assigns the time elapsed in seconds during trip 2 to to integer variable
    int CountsTrip1 = 1561; //Declares and assigns the number of rotations of the front wheel during trip 1 to an integer variable
    int CountsTrip2 = 9037; //Declares and assigns the number of rotations of the front wheel during trip 2 to an integer variable
    
    //Intermediate variables and output data
    double WheelDiameter = 27.0, //Declares and assigns the wheel diameter in inches to a double variable 
    Pi = 3.14159, //Declares and assigns the number π to a double variable 
    FeetPerMile = 5280, //Declares and assigns ft/mi convertion value to a double variable
    InchesPerFoot = 12, //Declares and assigns in/ft convertion value to a double variable
    SecondsPerMinute = 60; //Declares and assigns s/min convertion value to a double variable
    double DistanceTrip1, DistanceTrip2, TotalDistance; //Declares the distance for trip 1, trip 2 and the total distance as double variables
    
    //Prints out input data
    System.out.println("Trip 1 took " + (SecsTrip1/SecondsPerMinute) + " minutes and had " + CountsTrip1 + " rotation counts."); //Prints out the number of seconds (converted to minutes) and the rotation counts for trip 1
    System.out.println("Trip 2 took " + (SecsTrip2/SecondsPerMinute) + " minutes and had " + CountsTrip2 + " rotation counts."); //Prints out the number of seconds (converted to minutes) and the rotation counts for trip 2
    
    //Runs calculations and stores values
    DistanceTrip1 = CountsTrip1*WheelDiameter*Pi; //Multiplies accessed variables and stores the result as the distance for trip 1 (in inches)
    DistanceTrip1 /= InchesPerFoot*FeetPerMile; //Converts the distance of trip 1 from inches to miles and stores the result in DistanceTrip1
    DistanceTrip2 = ((CountsTrip2*WheelDiameter*Pi)/InchesPerFoot)/FeetPerMile; //Calculates the distance for trip 2, converts it from inches to miles and stores the result in DistanceTrip2
    TotalDistance = DistanceTrip1+DistanceTrip2; //Adds accessed variables and stores the result as the total distance for the two trips
    
    //Prints out output data
    System.out.println("Trip 1 was " + DistanceTrip1 + " miles."); //Prints out the distance for trip 1
	  System.out.println("Trip 2 was " + DistanceTrip2 + " miles."); //Prints out the distance for trip 2
	  System.out.println("The total distance was " + TotalDistance + " miles."); //Prints out total distance for the two trips
    
  } //End of Main Method
   
} //End of Class