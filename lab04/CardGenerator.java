//////////////
//// CSE2 Card Generator 
//// 02.15.19 Rafaela Mantoan Borges
//// Given that you are a magician and you need to practice your card tricks, this program should:
// Use the Math class to genarate a random number 
// Create two String variables (one corresponding to the name of the suit and the other corresponding to the identity of the card)
// Use if statements to assign the suit name
// Use a switch statement to assign the card identity
// Print out the name of the randomly selected card

//Import statement 
import java.lang.Math; //Imports Math class

public class CardGenerator {
  //Main method for every Java program 
  public static void main(String[] args) {
    
    //Generates a random number from 1 to 52 (inclusive)
    int RandomNumber = (int)(Math.random()*52)+1; //Uses the Math class to generate a random number for the card from 1 to 52 and declares it as an integer variable 
    
    //Creates two Strings
    String Suit = ""; //Declares and assigns the suit of the card to a string variable
    String Identity = ""; //Declares the identity of the card as a string variable
    
    //Assigns the suit name 
    if (RandomNumber <= 13) { //If the boolean expression that says that the random number for the card is less than or equal to 13 is true:
      Suit = "Diamonds"; //Assigns the suit of the card to diamonds
    } else if (14 <= RandomNumber && RandomNumber <= 26) { //If the boolean expression that says that the random number for the card is between 14 and 26 is true:
      Suit = "Clubs"; //Assigns the suit of the card to clubs
    } else if (27 <= RandomNumber && RandomNumber <= 39) { //If the boolean expression that says that the random number for the card is between 27 and 39 is true:
      Suit = "Hearts"; //Assigns the suit of the card to hearts 
    } else if (40 <= RandomNumber && RandomNumber <= 52) { //If the boolean expression that says that the random number for the card is between 40 and 
      Suit = "Spades"; //Assigns the suit of the card to spades 
    } //End of if statement
    
    //Assigns the card identity
    int Remainder = RandomNumber%13; //Calculates the remainder of the division between the random number and 13 and declares it as an integer variable
    switch (Remainder) { //Evaluates the value of the remainder 
      case 0: //If the remainder is 0:
        Identity = "King"; //Assigns the identity to king
        break; //Breaks out of the condition
      case 1: //If the remainder is 1:
        Identity = "Ace"; //Assigns the identity to ace 
        break; //Breaks out of the condition
      case 2: //If the remainder is 0:
        Identity = "2"; //Assigns the identity to two
        break; //Breaks out of the condition
      case 3: //If the remainder is 3:
        Identity = "3"; //Assigns the identity to three
        break; //Breaks out of the condition
      case 4: //If the remainder is 4:
        Identity = "4"; //Assigns the identity to four
        break;//Breaks out of the condition
      case 5: //If the remainder is 5:
        Identity = "5"; //Assigns the identity to five
        break; //Breaks out of the condition
      case 6: //If the remainder is 6:
        Identity = "6"; //Assigns the identity to six
        break; //Breaks out of the condition
      case 7: //If the remainder is 7:
        Identity = "7"; //Assigns the identity to seven
        break; //Breaks out of the condition
      case 8: //If the remainder is 8:
        Identity = "8"; //Assigns the identity to eight
        break; //Breaks out of the condition
      case 9: //If the remainder is 9:
        Identity = "9"; //Assigns the identity to nine
        break; //Breaks out of the condition
      case 10: //If the remainder is 10:
        Identity = "10"; //Assigns the identity to ten
        break; //Breaks out of the condition
      case 11: //If the remainder is 11:
        Identity = "Jack"; //Assigns the identity to jack
        break; //Breaks out of the condition
      case 12: //If the remainder is 12:
        Identity = "Queen"; //Assigns the identity to queen
        break; //Breaks out of the condition
    } //End of switch statement
    
    //Print out the result 
    System.out.println("You picked the " + Identity + " of " + Suit); //Print out the statement to the terminal window 
  
  } //End of main method

} //End of class