//////////////
//// CSE2 String Analysis
//// 03.26.19 Rafaela Mantoan Borges 
//// This program should: 
// Prompt the user for a string 
// Evaluate all the characters or just a specified number of characters in the string
// Determine whether they are letters or not

import java.util.Scanner; //Imports Scanner class (import statement)
import java.lang.Character; //Imports Character class (import statement)

public class StringAnalysis {
  
  //Method to analyze a string 
  public static boolean StringAnalyzer (String Value) { //The method accepts a string input and returns a boolean variable 
    boolean Condition = true; //Declares and initialize a boolean variable  
    int Length = Value.length(); //Counts the total number of characters and stores it in an integer variable 
    for (int Counter = 0; Counter < Length; Counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to length 
      char Symbol = Value.charAt(Counter); //Returns the character in position "counter" of the string
      if (Character.isLetter(Symbol)) { //If the character is a letter:
        System.out.println(Symbol + " = LETTER"); //Prints out the output to terminal window
      } else { //If the character is not a letter:
        System.out.println(Symbol + " = NOT A LETTER"); //Prints out the output to terminal window
        Condition = false; //Assigns condition to false
      } //End of if statement
    } //End of for loop
    return Condition; //Returns the boolean variable 
  } //End of StringAnalyzer method
  
  //Method to analyze part of a string 
  public static boolean StringAnalyzer (String Value, int Characters) { //The method accepts a string and an integer input and returns a boolean variable 
    boolean Condition = true; //Declares and initialize a boolean variable 
    int Length = Value.length(); //Counts the total number of characters and stores it in an integer variable 
    if (Characters < Length) { //If the number of characters is less than the length of the string:
      for (int Counter = 0; Counter < Characters; Counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to the number of characters
        char Symbol = Value.charAt(Counter); //Returns the character in position "counter" of the string
        if (Character.isLetter(Symbol)) { //If the character is a letter:
        System.out.println(Symbol + " = LETTER"); //Prints out the output to terminal window
        } else { //If the character is not a letter:
          System.out.println(Symbol + " = NOT A LETTER"); //Prints out the output to terminal window
          Condition = false; //Prints out the output to terminal window
        } //End of if statement
      } //End of for loop
    } else if (Characters > Length) { //If the number of characters is greater than the length of the string:
      for (int Counter = 0; Counter < Length; Counter++) { //Declares and inizializes a counter incrementing it by 1 until it is equal to length 
        char Symbol = Value.charAt(Counter); //Returns the character in position "counter" of the string
        if (Character.isLetter(Symbol)) { //If the character is a letter:
          System.out.println(Symbol + " = LETTER"); //Prints out the output to terminal window
        } else { //If the character is not a letter:
          System.out.println(Symbol + " = NOT A LETTER"); //Prints out the output to terminal window
          Condition = false; //Prints out the output to terminal window
        } //End of if statement
      } //End of for loop
    } //End of if statement
    return Condition; //Returns the boolean variable 
  } //End of StringAnalyzer method
  
  //Main method required for every Java program
  public static void main (String[] args) {
    
    //Declaration and construction
    Scanner myScanner = new Scanner(System.in); //Declares and constructs an instance
    
    //String 
    System.out.print("Enter a String: "); //Prompts the user for a string 
    boolean Condition1 = myScanner.hasNext(); //Declares and assigns a boolean to check if the input is available to be read
    String Value = "";  //Declares and assigns to a string 
    
    if (Condition1) { //If input is available to be read (if the condition is true):
      Value = myScanner.next(); //Accepts and returns the string value
    } else { //If the condition is false (input is not available to be read):
      while (Condition1 == false) { //While the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF STRING TYPE NEEDED"); //Prints out the error message to the terminal window 
        String junkWord1 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter a String: "); //Prompts the user for a string
        Condition1 = myScanner.hasNext(); //Checks if the input is available to be read
        if (Condition1) { //If input is available to be read (if the condition is true):
          Value = myScanner.next(); //Accepts and returns the string value
        } //End of if statement 
      } //End of while loop       
    } //End of if statement
    
    //Number of characters
    System.out.print("Enter the number of characters you'd like to analyze: "); //Prompts the user for the number of characters
    boolean Condition2 = myScanner.hasNextInt(); //Declares and assigns a boolean to check if the input is available to be read
    int Characters = 0; //Declares and assigns the number of characters to an integer  
    
    if (Condition2) { //If input is available to be read (if the condition is true):
      Characters = myScanner.nextInt(); //Accepts and returns the integer value  
    } //End of if statement
    do { //Does while the condition is false (input is not available to be read) or the number of characters is less than or equal to 0:
      if (Condition2) { //If input is available to be read (if the condition is true):
        if (Characters <= 0) { //If the number of characters is less than or equal to 0:
          System.out.println("ERROR: POSITIVE INTEGER NEEDED"); //Prints out the error message to the terminal window 
          System.out.print("Enter the number of characters you'd like to analyze: "); //Prompts the user for the number of characters
          Condition2 = myScanner.hasNextInt(); //Checks if the input is available to be read
          if (Condition2) { //If input is available to be read (if the condition is true):
            Characters = myScanner.nextInt(); //Accepts and returns the integer value  
        } //End of if statement
      } //End of if statement
    } else { //If the condition is false (input is not available to be read):
        System.out.println("ERROR: INPUT OF INTEGER TYPE NEEDED"); //Prints out the error message to the terminal window
        String junkWord2 = myScanner.next(); //Declares and assigns a string to remove what was typed by the user 
        System.out.print("Enter the number of characters you'd like to analyze: "); //Prompts the user for the number of characters
        Condition2 = myScanner.hasNextInt(); //Checks if the input is available to be read
        if (Condition2) { //If input is available to be read (if the condition is true):
          Characters = myScanner.nextInt(); //Accepts and returns the integer value 
        } //End of if statement
      } //End of if statement 
    } while ((Condition2 == false) || (Characters <= 0)); //End of do-while loop
    
    
    if (Characters == Value.length()) { //If the number of characters is equal to the lenght of the string 
      boolean Analysis = StringAnalyzer(Value); //Calls StringAnalyzer method and stores it in a boolean variable
      if (Analysis) { //If the boolean is true (if the all characters are letters):
        System.out.println("The String is all letters"); //Prints out the statement to terminal window
      } else { //If the boolean is false (if not all characters are letters):
        System.out.println("The String has characters that are not letters"); //Prints out the statement to terminal window
      } //End of if statement 
    } else { //If the number of characters is different than the lenght of the string:
      boolean Analysis = StringAnalyzer(Value, Characters); //Calls StringAnalyzer method and stores it in a boolean variable
      if (Analysis) { //If the boolean is true (if the all characters are letters):
        System.out.println("The part of the String evaluated is just letters"); //Prints out the statement to terminal window
      } else { //If the boolean is false (if not all characters are letters):
        System.out.println("The part of the String evaluated has characters that are not letters"); //Prints out the statement to terminal window
      } //End of if statement 
    } //End of if statement
    
  } //End of main method
  
} //End of class